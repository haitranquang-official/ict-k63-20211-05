import java.util.Scanner;

public class Calculation {
	private int a;
	private int b;
	
	public int add() {
		return a + b;
	}
	
	public int sub() {
		return a - b;
	}
	
	public int mul() {
		return a*b;
	}
	
	public int div() {
		return a/b;
	}
	
	public int modulo() {
		return a%b;
	}
	
	public Calculation(int a, int b) {
		super();
		this.a = a;
		this.b = b;
	}

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Calculate addition, subtraction, mutiplication, division of two number a and b!");
		System.out.print("Enter a: ");
		int a = s.nextInt();
		System.out.print("Enter b: ");
		int b = s.nextInt();
		Calculation c = new Calculation(a, b);
		
		System.out.println("a + b = " + c.add());
		System.out.println("a - b = " + c.sub());
		System.out.println("a * b = " + c.mul());
		System.out.println("a / b = " + c.div());
		s.close();
	}

}
