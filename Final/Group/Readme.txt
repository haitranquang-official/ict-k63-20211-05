                            PROJECT SUMMARY

* Group number: 5

* Members:
    Nguyen Hoang Vu      - 20190100
    Tran Quang Hai       - 20194755
    Nguyen Duy Tien      - 20194857
    Tong Thi Thu Anh     - 20194728
    Nguyen Thi Thu Giang - 20194750

* Assignments:
    * Vu: 20%
        - Use case: Create bike rental.
        - Design, develop and test logic and UI for above use case. 
        - Design structure packages, classes of the project with Tien.
        - Merge with Tien to form a complete User Rent Bike functionality.
    * Hai: 20%
	- Main use case: Create Parking Lot
	- Develop the login interface.
	- Develop basic version of SwitchScreenUtil and ContextHolder for the application.
	- Develop the view parking lot interface for admin.
	- Develop the main screen for admin with Giang.
    * Tien: 20%
	- Main use case: Return bike
	- Modify and summarize the general package diagram
	- Develop bank subsystem with Nguyen Hoang Vu
	- Implement user home screen 
	- Give ideas for design priciples
	- Contribute to other groupwork and help teammates
    * Thu Anh: 20%
	- Use case: Search Parking Lot and View Parking Lot
    	- Design, develop above use case, design unit test for Search Parking Lot
    	- Work with Hai to design Admin Search Parking Lot
    	- Work with Giang to design Admin View Bike
    * Giang: 20%
	- Use case: CRUD bike of admin, main use case is Add New Bike
	- Design, develop above use case, design unit test for Add New Bike
	- Develop the admin home screen with Hai	
	- Update database

* Evaluation:
    * Teamwork: great teamwork. There were some misunderstandings during the development progress, but are resolved smoothly.
        * Vu: Leader, Manage distribution of task to team members. Provided brilliant ideas about packages and classes structure design. Supported team members in coding problem.
        * Hai: hard-working, disciplinal. Finished assigned tasks on time, provided many experienced ideas in solving design and coding problem.
        * Tien: lhard-working, disciplinal. Finished many difficult tasks such as creating database, drawing design class diagram of the project, ... 
        * Thu Anh: hard-working, disciplinal. Finished assigned tasks on time. Be responsible for combining and perfecting group report. 
	* Giang: hard-working. Struggled to follow and cooperate at first but managed to finish her work very well.
    * Project:
        * Finish on-time.
        * All required functionalities are analyzed and implemented correctly.
        * Add a small functionality to improve user experience.
        * Code is organized logically, broad test cases covering all possibilities.

* Project Guideline: