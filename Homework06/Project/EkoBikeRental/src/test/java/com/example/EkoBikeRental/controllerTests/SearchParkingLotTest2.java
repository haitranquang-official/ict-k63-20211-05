package com.example.EkoBikeRental.controllerTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.EkoBikeRental.EkoBikeRentalApplication;
import com.example.EkoBikeRental.abstracts.SearchParkingLotController;
import com.example.EkoBikeRental.abstracts.SearchParkingLotService;
import com.example.EkoBikeRental.entities.Bike;
import com.example.EkoBikeRental.entities.NormalBike;
import com.example.EkoBikeRental.entities.ParkingLot;
import com.example.EkoBikeRental.user.parkinglot.UserSearchParkingLotController;

import javafx.collections.FXCollections;
import javafx.collections.transformation.FilteredList;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EkoBikeRentalApplication.class)
public class SearchParkingLotTest2 {
	
    @Autowired
    public SearchParkingLotService searchParkingLotService;
    
    @Autowired
    public UserSearchParkingLotController searchParkingLotController;
    
    FilteredList<ParkingLot> filteredList;
    
    @BeforeEach
    public void setFilter() {
    	filteredList = new FilteredList<>(FXCollections.observableArrayList(searchParkingLotService.findAll()));
    }
    
    @Test
    public void testFilter1() {
    	String searchString = "";
    	boolean searchByName = true;
    	searchParkingLotController.setPredicate(filteredList, searchString, searchByName);
    	
    	assertTrue(filteredList.size() > 0);
    	assertEquals(filteredList.size(), searchParkingLotService.findAll().size());
    }
    
    @Test
    public void testFilter2() {
    	String searchString = "Ba";
    	boolean searchByName = true;
    	searchParkingLotController.setPredicate(filteredList, searchString, searchByName);
    	
    	assertTrue(filteredList.size() > 0);
    
    	boolean checkTrue = true; 
    	
    	for (ParkingLot pl : filteredList) {
    		if (!pl.getName().toLowerCase().contains(searchString.toLowerCase())) {
    			checkTrue = false;
    		}
    	}
    	assertTrue(checkTrue);
    }
    
    @Test
    public void testFilter3() {
    	String searchString = "CO";
    	boolean searchByName = false; //searchByAddress
    	searchParkingLotController.setPredicate(filteredList, searchString, searchByName);
    	
    	assertTrue(filteredList.size() > 0);
    
    	boolean checkTrue = true; 
    	
    	for (ParkingLot pl : filteredList) {
    		if (!pl.getAddress().toLowerCase().contains(searchString.toLowerCase())) {
    			checkTrue = false;
    		}
    	}
    	assertTrue(checkTrue);
    }
    
    @Test
    public void testFilter() {
    	String searchString = "z";
    	boolean searchByName = true; //false is search by address
    	searchParkingLotController.setPredicate(filteredList, searchString, searchByName);
    	
    	assertTrue(filteredList.size() == 0);
    }
    
}
