package com.example.EkoBikeRental.serviceTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.transaction.Transactional;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import com.example.EkoBikeRental.EkoBikeRentalApplication;
import com.example.EkoBikeRental.admin.bike.service.BikeCheckInfoService;
import com.example.EkoBikeRental.admin.bike.service.BikeCheckPLService;
import com.example.EkoBikeRental.admin.bike.service.BikeUpdatePLService;
import com.example.EkoBikeRental.admin.bike.service.BikeUpdateService;
import com.example.EkoBikeRental.entities.Bike;
import com.example.EkoBikeRental.entities.NormalBike;
import com.example.EkoBikeRental.entities.ParkingLot;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EkoBikeRentalApplication.class)
public class AminBikeServiceTest {
	
	@Autowired
	BikeCheckInfoService bikeCheckInfoService;
	
	@Autowired
	BikeUpdateService bikeUpdateBikeService;
	
	@Autowired
	BikeCheckPLService bikeCheckPLService;
	
	@Autowired
	BikeUpdatePLService bikeUpdatePLService;
	
	private Bike bike;
	
	@BeforeEach
	public void addANewBike() {
		bike = new NormalBike();
		bike.setName("Honda 123");
        bike.setLicensePlate("12345");
        bike.setProducer("Honda");
	}
	
	@Test
    @Transactional
    public void testAddNewBike() throws Exception { 
		bikeUpdateBikeService.updateBikeDb(bike);
        
        Bike lastAddedBike = bikeUpdateBikeService.findAll().get(bikeUpdateBikeService.findAll().size() - 1);
        
        assertEquals(lastAddedBike, bike);
    }
	
	@Test
	@Transactional
	public void testValidLicense() throws Exception {
		String testLicense = "LP9000";
		
		assertDoesNotThrow(() -> bikeCheckInfoService.checkNotEmptyLicense(testLicense));
		assertDoesNotThrow(() -> bikeCheckInfoService.checkLicenseExist(testLicense));
        
		bike.setLicensePlate(testLicense);
		bikeUpdateBikeService.updateBikeDb(bike);
        
        Bike lastAddedBike = bikeCheckInfoService.findAll().get(bikeCheckInfoService.findAll().size() - 1);
        
        String bikeLicense = lastAddedBike.getLicensePlate();
        assertEquals(bikeLicense, testLicense);
	}
	
	@Test
	public void testInvalidLicense() {
      String testLicense2 = "";
      Exception nullError = assertThrows(Exception.class, () -> bikeCheckInfoService.checkNotEmptyLicense(testLicense2.toString()));
      assertTrue(nullError.getMessage().contains("not be empty"));
      
      String testLicense3 = bikeCheckInfoService.findAll().get(0).getLicensePlate();
      Exception duplicateError = assertThrows(Exception.class, () -> bikeCheckInfoService.checkLicenseExist(testLicense3.toString()));
      assertTrue(duplicateError.getMessage().contains("already exists"));
	}
	
	@Test
	@Transactional
	public void testValidParkingLot() throws Exception {
		ParkingLot testParkingLot = bikeCheckPLService.findAll().get(0);
		
		assertDoesNotThrow(() -> bikeCheckPLService.checkNotNull(testParkingLot));
		assertDoesNotThrow(() -> bikeCheckPLService.checkFullSlot(testParkingLot));
        
		bike.setParkingLot(testParkingLot);
		bikeUpdateBikeService.updateBikeDb(bike);
        
        Bike lastAddedBike = bikeCheckInfoService.findAll().get(bikeCheckInfoService.findAll().size() - 1);
        
        ParkingLot bikeParkingLot = lastAddedBike.getParkingLot();
        assertEquals(testParkingLot, bikeParkingLot);
	}
	
	@Test
	public void testInvalidParkingLot() {
      ParkingLot testParkingLot1 = null;
      Exception nullError = assertThrows(Exception.class, () -> bikeCheckPLService.checkNotNull(testParkingLot1));
      assertTrue(nullError.getMessage().contains("not be empty"));
      
      ParkingLot testParkingLot2 = bikeCheckPLService.findAll().get(0);
      testParkingLot2.setNumBike(testParkingLot2.getNumberOfSlots());
      Exception duplicateError = assertThrows(Exception.class, () -> bikeCheckPLService.checkFullSlot(testParkingLot2));
      assertTrue(duplicateError.getMessage().contains("already full"));
	}
	
	@Test
    @Transactional
    public void testdWeight() throws Exception { 
		bike.setWeight(3.0);
		bikeUpdateBikeService.updateBikeDb(bike);
        
        Bike lastAddedBike = bikeCheckInfoService.findAll().get(bikeCheckInfoService.findAll().size() - 1);
        
        Double bikeWeight = lastAddedBike.getWeight();
        Double testWeight1 = 3.0;
        assertEquals(bikeWeight, testWeight1);
        
        Double testWeight2 = -3.0;    
        Exception throwError = assertThrows(Exception.class, () -> bikeCheckInfoService.checkValidWeight(testWeight2.toString()));
        assertTrue(throwError.getMessage().contains("must be greater than 0"));
    }
}
