package com.example.EkoBikeRental.controllerTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.EkoBikeRental.EkoBikeRentalApplication;
import com.example.EkoBikeRental.abstracts.SearchParkingLotController;
import com.example.EkoBikeRental.abstracts.SearchParkingLotService;
import com.example.EkoBikeRental.entities.ParkingLot;
import com.example.EkoBikeRental.user.parkinglot.UserSearchParkingLotController;

import javafx.collections.FXCollections;
import javafx.collections.transformation.FilteredList;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EkoBikeRentalApplication.class)
public class SearchParkingLotTest1 {
	
    @Autowired
    public SearchParkingLotService searchParkingLotService;
    
    @Autowired
    public UserSearchParkingLotController searchParkingLotController;

    @Test
    public void testSearchParkingLotBlackBox1() {
    	FilteredList<ParkingLot> filteredList = new FilteredList<>(FXCollections.observableArrayList(searchParkingLotService.findAll()));
    	String searchString = "d3";
    	boolean searchByName = true;
    	searchParkingLotController.setPredicate(filteredList, searchString, searchByName);
    	
    	assertTrue(filteredList.size() > 0);

    	boolean checkTrue = true; //check if all parking lots in filteredList contain searchString
    	
    	for (ParkingLot pl : filteredList) {
    		if (!pl.getName().toLowerCase().contains(searchString.toLowerCase())) {
    			checkTrue = false;
    		}
    	}
    	assertTrue(checkTrue);
    	
    
    	List<ParkingLot> remainList = searchParkingLotService.findAll();
    	
    	for (ParkingLot pl : filteredList) {
    		for (int i = 0; i < remainList.size(); i++) {
    			if(remainList.get(i).getId() == pl.getId()) { 
    				remainList.remove(i);
    			}
    		}
    	}
    	
    	boolean checkFalse = false; //check if all parking lots not in filteredList contain searchString
    	
    	for (ParkingLot pl : remainList) {
    		if (pl.getName().toLowerCase().contains(searchString.toLowerCase())) {
    			checkFalse = true;
    		}
    	}
    	assertFalse(checkFalse);
    }
    
    @Test
    public void testParkingLotBlackBox2() {
    	FilteredList<ParkingLot> filteredList = new FilteredList<>(FXCollections.observableArrayList(searchParkingLotService.findAll()));
    	String searchString = "gi";
    	boolean searchByName = false; //searchByAddress
    	searchParkingLotController.setPredicate(filteredList, searchString, searchByName);
    	
    	assertTrue(filteredList.size() > 0);
    
    	boolean checkTrue = true; //check if all parking lots in filteredList contain searchString
    	
    	for (ParkingLot pl : filteredList) {
    		if (!pl.getAddress().toLowerCase().contains(searchString.toLowerCase())) {
    			checkTrue = false;
    		}
    	}
    	assertTrue(checkTrue);
    	
    
    	List<ParkingLot> remainList = searchParkingLotService.findAll();
    	
    	for (ParkingLot pl : filteredList) {
    		for (int i = 0; i < remainList.size(); i++) {
    			if(remainList.get(i).getId() == pl.getId()) { 
    				remainList.remove(i);
    			}
    		}
    	}
    	
    	boolean checkFalse = false; //check if all parking lots not in filteredList contain searchString
    	
    	for (ParkingLot pl : remainList) {
    		if (pl.getAddress().toLowerCase().contains(searchString.toLowerCase())) {
    			checkFalse = true;
    		}
    	}
    	assertFalse(checkFalse);
    }
}
