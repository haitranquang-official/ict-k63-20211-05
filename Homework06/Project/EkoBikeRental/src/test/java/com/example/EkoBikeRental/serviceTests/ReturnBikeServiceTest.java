package com.example.EkoBikeRental.serviceTests;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;

import com.example.EkoBikeRental.EkoBikeRentalApplication;
import com.example.EkoBikeRental.entities.Bike;
import com.example.EkoBikeRental.entities.BikeStatus;
import com.example.EkoBikeRental.entities.NormalBike;
import com.example.EkoBikeRental.entities.Record;
import com.example.EkoBikeRental.entities.TwinBike;
import com.example.EkoBikeRental.repositories.BikeRepository;
import com.example.EkoBikeRental.repositories.RecordRepository;
import com.example.EkoBikeRental.user.bike.rental.rent.RentBikeService;
import com.example.EkoBikeRental.user.bike.rental.returnbike.ReturnBikeService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EkoBikeRentalApplication.class)

public class ReturnBikeServiceTest {
    @Autowired
    ReturnBikeService returnBikeService;

    @Autowired
    RentBikeService rentBikeService;

    @Autowired
    RecordRepository recordRepository;

    @Autowired
    BikeRepository bikeRepository;

    @Test
    @Transactional
    public void testCalculateRentalFee() {
        Record record = new Record();
        Bike twinBike = new TwinBike();

        record.setBike(twinBike);

        Integer rentalTime = 70;
        record.setStartTime(LocalDateTime.now().minusMinutes(rentalTime));

        Double rentalFee = returnBikeService.calculateRentalFee(record);
        Double testRentalFee = 28500.0;

        assertEquals(testRentalFee, rentalFee);
    }

    @Test
    @Transactional
    public void testCalculateRentalFeeLessThan10Mins() {
        Record record = new Record();
        Bike twinBike = new TwinBike();

        record.setBike(twinBike);

        Integer rentalTime = 5;
        record.setStartTime(LocalDateTime.now().minusMinutes(rentalTime));

        Double rentalFee = returnBikeService.calculateRentalFee(record);

        assertEquals(0, rentalFee);
    }

    @Test
    @Transactional
    public void testFindBikeByBikeID() throws Exception {
        Bike bike = new NormalBike();
        bike.setBikeStatus(BikeStatus.BUSY);
        bikeRepository.save(bike);

        Bike testBike = returnBikeService.findBikeByBikeID(bike.getId());
        assertEquals(bike.toString(), testBike.toString());
    }

    @Test
    @Transactional
    public void testFindBikeByBikeIDNotSatisfy() throws Exception {
        Bike bike = new NormalBike();
        bike.setBikeStatus(BikeStatus.FREE);
        bikeRepository.save(bike);

        // Bike testBike = returnBikeService.findBikeByBikeID(bike.getId());
        
        Exception throwNotSatisy = assertThrows(Exception.class, () -> returnBikeService.findBikeByBikeID(bike.getId()));
        assertTrue(throwNotSatisy.getMessage().contains("not renting"));
    }

    @Test
    @Transactional
    public void testGetCurrentRentalTime() {
        Record record = new Record();
        Integer rentalTime = 60 * 3;
        record.setStartTime(LocalDateTime.now().minusMinutes(rentalTime));
        Integer testRentalTime = returnBikeService.getCurrentRentalTime(record);

        Integer difference = Math.abs(testRentalTime - rentalTime);
        assertTrue(difference == 0);
    }
}
