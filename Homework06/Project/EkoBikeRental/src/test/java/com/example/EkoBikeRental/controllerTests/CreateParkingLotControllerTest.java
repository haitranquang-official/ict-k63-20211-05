package com.example.EkoBikeRental.controllerTests;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.transaction.Transactional;

import com.example.EkoBikeRental.EkoBikeRentalApplication;
import com.example.EkoBikeRental.admin.parkinglot.create.CreateParkingLotController;
import com.example.EkoBikeRental.entities.ParkingLot;
import com.example.EkoBikeRental.repositories.ParkingLotRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javafx.event.ActionEvent;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EkoBikeRentalApplication.class)
public class CreateParkingLotControllerTest {
    @Autowired
    CreateParkingLotController createParkingLotController;

    @Autowired
    ParkingLotRepository parkingLotRepository;

    @Test
    @Transactional
    public void addParkingLot() throws Exception
    {
        int old = parkingLotRepository.findAll().size();
        ParkingLot parkingLot = new ParkingLot();
        parkingLot.setName("Hihi");
        parkingLot.setAddress("Hehehe");
        parkingLot.setDescription("Really good");
        parkingLot.setNumberOfSlots(5);
        createParkingLotController.addParkingLot(parkingLot);
        assertEquals(parkingLotRepository.findAll().size(), old+1);
        List<ParkingLot> parkingLots = this.parkingLotRepository.findAll();
        ParkingLot newParkingLot = parkingLots.get(parkingLots.size() - 1);
        assertEquals(newParkingLot.getAddress(), parkingLot.getAddress());
        assertEquals(newParkingLot.getName(), parkingLot.getName());
        assertEquals(newParkingLot.getNumberOfSlots(), parkingLot.getNumberOfSlots());
        assertEquals(newParkingLot.getDescription(), parkingLot.getDescription());
    }

}
