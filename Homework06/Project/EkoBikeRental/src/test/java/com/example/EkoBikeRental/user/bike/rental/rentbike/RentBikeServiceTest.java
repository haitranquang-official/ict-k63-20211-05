package com.example.EkoBikeRental.user.bike.rental.rentbike;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;

import com.example.EkoBikeRental.EkoBikeRentalApplication;
import com.example.EkoBikeRental.entities.Bike;
import com.example.EkoBikeRental.entities.BikeStatus;
import com.example.EkoBikeRental.entities.NormalBike;
import com.example.EkoBikeRental.repositories.BikeRepository;
import com.example.EkoBikeRental.user.bike.rental.rent.RentBikeService;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EkoBikeRentalApplication.class)
public class RentBikeServiceTest {

    @Autowired
    private RentBikeService rentBikeService;

    @Autowired
    private BikeRepository bikeRepository;

    @Test
    @Transactional
    public void rentBike() throws Exception{
        int oldNum = bikeRepository.findAll().size();
        Bike bike = new NormalBike();
        bike.setBikeStatus(BikeStatus.FREE);
        bike.setCost(5000.0);
        bike.setLicensePlate("127371264");
        bike.setManufacturingDate(LocalDate.now());
        bike.setName("hello123");
        bike.setProducer("honda");

        bikeRepository.save(bike);
        int newNum = bikeRepository.findAll().size();
        Long bikeID = bike.getId();
        assertEquals(oldNum + 1, newNum);
        // check bikeID validation
        Bike newBike = rentBikeService.findBikeByBikeID(bikeID);
        assertEquals(bike, newBike);
        
    }

    @Test
    @Transactional
    public void testFindBikeByBikeIDNotSatisfy() throws Exception {
        Bike bike = new NormalBike();
        bike.setBikeStatus(BikeStatus.FREE);
        bike.setCost(5000.0);
        bike.setLicensePlate("127371264");
        bike.setManufacturingDate(LocalDate.now());
        bike.setName("hello123");
        bike.setProducer("honda");

        bikeRepository.save(bike);

        // Bike testBike = returnBikeService.findBikeByBikeID(bike.getId());
        
        Exception throwNotSatisy = assertThrows(Exception.class, () -> rentBikeService.findBikeByBikeID(500l));
        System.out.println(throwNotSatisy.getMessage());
        assertTrue(throwNotSatisy.getMessage().contains("Bike not found"));
    }

    @Test
    @Transactional
    public void testCheckBikeStatuswithBikeBUSY() throws Exception{
        Bike bike = new NormalBike();
        bike.setBikeStatus(BikeStatus.BUSY);
        bike.setCost(5000.0);
        bike.setLicensePlate("127371264");
        bike.setManufacturingDate(LocalDate.now());
        bike.setName("hello123");
        bike.setProducer("honda");

        bikeRepository.save(bike);
        Long bikeID = bike.getId();
        
        // check bikeID validation
        Bike newBike = rentBikeService.findBikeByBikeID(bikeID);
        assertEquals(bike, newBike);

        BikeStatus bikeStatus = bike.getBikeStatus();
        assertEquals(bikeStatus, rentBikeService.checBikeStatus(newBike));
    }



}
