package com.example.EkoBikeRental;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.example.EkoBikeRental.entities.User;
import com.example.EkoBikeRental.entities.UserStatus;
import com.example.EkoBikeRental.repositories.BikeRepository;
import com.example.EkoBikeRental.repositories.UserRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = EkoBikeRentalApplication.class)
public class TestFile {
    @Autowired
    UserRepository userRepository;
    
    @Test
    @Transactional
    public void saveUser(){
        int old = userRepository.findAll().size();
        User user = new User();
        user.setAccount("user4");
        user.setPassword("123");
        user.setStatus(UserStatus.NOT_RENTING);
        userRepository.save(user);
        assertEquals(userRepository.findAll().size(), old+1);
    }
}
