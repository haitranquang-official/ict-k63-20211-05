package com.example.EkoBikeRental.serviceTests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import javax.transaction.Transactional;

import com.example.EkoBikeRental.EkoBikeRentalApplication;
import com.example.EkoBikeRental.admin.parkinglot.create.CreateParkingLotController;
import com.example.EkoBikeRental.admin.parkinglot.create.CreateParkingLotService;
import com.example.EkoBikeRental.entities.ParkingLot;
import com.example.EkoBikeRental.repositories.ParkingLotRepository;
import com.example.EkoBikeRental.util.AccountType;
import com.example.EkoBikeRental.util.ContextHolder;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EkoBikeRentalApplication.class)
public class CreateParkingLotServiceTest {
    @Autowired
    CreateParkingLotService createParkingLotService;

    @Autowired
    ParkingLotRepository parkingLotRepository;

    public void setAccountTypeAdmin()
    {
        ContextHolder.setAccountType(AccountType.ADMIN);
    }

    @Test
    @Transactional
    public void addParkingLot()
    {
        setAccountTypeAdmin();
        int old = parkingLotRepository.findAll().size();
        ParkingLot parkingLot = new ParkingLot();
        parkingLot.setName("Hihi");
        parkingLot.setAddress("Hehehe");
        parkingLot.setDescription("Really good");
        parkingLot.setNumberOfSlots(5);
        try {
            createParkingLotService.addParkingLot(parkingLot);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        assertEquals(parkingLotRepository.findAll().size(), old+1);
        List<ParkingLot> parkingLots = this.parkingLotRepository.findAll();
        ParkingLot newParkingLot = parkingLots.get(parkingLots.size() - 1);
        assertEquals(newParkingLot.getAddress(), parkingLot.getAddress());
        assertEquals(newParkingLot.getName(), parkingLot.getName());
        assertEquals(newParkingLot.getNumberOfSlots(), parkingLot.getNumberOfSlots());
        assertEquals(newParkingLot.getDescription(), parkingLot.getDescription());
    }

    @Test
    @Transactional
    public void addParkingLotWithDuplicateName()
    {
        setAccountTypeAdmin();
        int veryOldSize = parkingLotRepository.findAll().size();
        ParkingLot parkingLot = new ParkingLot();
        parkingLot.setName("Hihi");
        parkingLot.setAddress("Hehehe");
        parkingLot.setDescription("Really good");
        parkingLot.setNumberOfSlots(5);
        try {
            createParkingLotService.addParkingLot(parkingLot);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        int oldSize = parkingLotRepository.findAll().size();
        assertEquals(veryOldSize + 1, oldSize);
        ParkingLot newParkingLot = new ParkingLot();
        newParkingLot.setName("Hihi");
        newParkingLot.setAddress("Hehehe");
        newParkingLot.setDescription("Really good");
        newParkingLot.setNumberOfSlots(5);
        try {
            createParkingLotService.addParkingLot(parkingLot);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        int newSize = parkingLotRepository.findAll().size();
        assertEquals(oldSize, newSize);
    }

    @Test
    @Transactional
    public void addParkingLotWithNegativeNumberOfSlot()
    {
        setAccountTypeAdmin();
        int veryOldSize = parkingLotRepository.findAll().size();
        ParkingLot parkingLot = new ParkingLot();
        parkingLot.setName("Hihi");
        parkingLot.setAddress("Hehehe");
        parkingLot.setDescription("Really good");
        parkingLot.setNumberOfSlots(-5);
        try {
            createParkingLotService.addParkingLot(parkingLot);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        int oldSize = parkingLotRepository.findAll().size();
        assertEquals(veryOldSize, oldSize);
    }

    @Test
    @Transactional
    public void addParkingLotWithDuplicateNameAndNegativeSlots()
    {
        setAccountTypeAdmin();
        int veryOldSize = parkingLotRepository.findAll().size();
        ParkingLot parkingLot = new ParkingLot();
        parkingLot.setName("Hihi");
        parkingLot.setAddress("Hehehe");
        parkingLot.setDescription("Really good");
        parkingLot.setNumberOfSlots(5);
        try {
            createParkingLotService.addParkingLot(parkingLot);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        int oldSize = parkingLotRepository.findAll().size();
        assertEquals(veryOldSize + 1, oldSize);
        ParkingLot newParkingLot = new ParkingLot();
        newParkingLot.setName("Hihi");
        newParkingLot.setAddress("Hehehe");
        newParkingLot.setDescription("Really good");
        newParkingLot.setNumberOfSlots(-5);
        try {
            createParkingLotService.addParkingLot(parkingLot);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        int newSize = parkingLotRepository.findAll().size();
        assertEquals(oldSize, newSize);
    }
    
}


