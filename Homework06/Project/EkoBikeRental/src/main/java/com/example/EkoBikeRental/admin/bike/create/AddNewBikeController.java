package com.example.EkoBikeRental.admin.bike.create;

import javax.swing.JOptionPane;

import org.springframework.stereotype.Component;

import com.example.EkoBikeRental.admin.bike.AdminABikePageController;
import com.example.EkoBikeRental.entities.Bike;
import com.example.EkoBikeRental.entities.BikeType;
import com.example.EkoBikeRental.entities.EBike;
import com.example.EkoBikeRental.entities.NormalBike;
import com.example.EkoBikeRental.entities.TwinBike;
import javafx.event.ActionEvent;

@Component
public class AddNewBikeController extends AdminABikePageController{
    
	@Override
	protected void setText() {
		titleLbl.setText("Add New Bike");
		funcBtn.setText("Add");
	}

	@Override
	protected void setBikeType() {
		typeComboBox.setValue(BikeType.NORMAL_BIKE);
	}

	@Override
	protected void checkValidity() throws Exception {
		try {
			checkValidLicense(licenseTf.getText());
			checkValidParkingLot(parkingLotComboBox.getValue());
	    	checkNotRequiredInfo();
	    } catch (Exception e) {
	        JOptionPane.showMessageDialog(null, e.getMessage(), "Alert", JOptionPane.WARNING_MESSAGE);
	        throw new Exception();
	    }
	}
	
	@Override
	protected void funcBtnHandler(ActionEvent event) {
		addBikeHandler(event);
	}
    
    
    private void addBikeHandler(ActionEvent event) {
    	try {
    		checkValidity();
    	} catch(Exception e) {
    		return;
    	}
    	bike = createNewBike(typeComboBox.getValue());
        setBikeInfo();
        String question = new String("Are you sure to add this new bike?");
        String message = new String("Successfully added a new bike.\n");
        checkConfirmation(event, bike, question, message);
    }
    
	private Bike createNewBike(BikeType type) {
    	switch(type) {
    		case NORMAL_BIKE:
    			return new NormalBike();
    		case ELECTRICAL_BIKE:
    			return new EBike();
    		case TWIN_BIKE:
    			return new TwinBike();
    		default:
    			return null;
    	}
    }

	@Override
	protected void updateDatabase() throws Exception {
		bikeUpdatePLService.addABikeToParkingLot(bike.getParkingLot());
		bikeUpdateService.updateBikeDb(bike);
	}
    
}
