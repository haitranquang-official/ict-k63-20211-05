package com.example.EkoBikeRental.bank;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class BankAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "account_num")
    private String accountNum;
    private String password;

    private Double balance = 1000000.0;

    public BankAccount() {}

    public BankAccount(Long id, String accountNum, String password, Double balance) {
        this.id = id;
        this.accountNum = accountNum;
        this.password = password;
        this.balance = balance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getAccountNum() {
        return this.accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public BankAccount id(Long id) {
        setId(id);
        return this;
    }

    public BankAccount accountNum(String accountNum) {
        setAccountNum(accountNum);
        return this;
    }

    public BankAccount password(String password) {
        setPassword(password);
        return this;
    }

    public BankAccount balance(Double balance) {
        setBalance(balance);
        return this;
    }

    @Override
    public String toString() {
        return "{" +
                " id='" + getId() + "'" +
                ", accountNum='" + getAccountNum() + "'" +
                ", password='" + getPassword() + "'" +
                ", balance='" + getBalance() + "'" +
                "}";
    }

}
