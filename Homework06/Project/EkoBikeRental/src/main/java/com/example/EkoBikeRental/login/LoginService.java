package com.example.EkoBikeRental.login;

import javax.swing.JOptionPane;

import com.example.EkoBikeRental.entities.Admin;
import com.example.EkoBikeRental.entities.User;
import com.example.EkoBikeRental.repositories.AdminRepository;
import com.example.EkoBikeRental.repositories.UserRepository;
import com.example.EkoBikeRental.util.AccountType;
import com.example.EkoBikeRental.util.ContextHolder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {
    private final UserRepository userRepository;
    private final AdminRepository adminRepository;
    @Autowired
    public LoginService(UserRepository userRepository, AdminRepository adminRepository)
    {
        this.adminRepository = adminRepository;
        this.userRepository = userRepository;
    }
    public User correctUserLogin(String account, String password)
    {
        User user = this.userRepository.findOneByAccount(account);
        if ((user!=null) && (user.getPassword().equals(password)))
        {
            ContextHolder.setID(user.getId());
            ContextHolder.setAccountType(AccountType.USER);
            return user;
        }
        else 
        {
            JOptionPane.showMessageDialog(null, "Invalid credentials, try again");
            return null;
        }
    }
    public Admin correctAdminLogin(String account, String password)
    {
        Admin admin = this.adminRepository.findOneByAccount(account);
        if ((admin!=null) && (admin.getPassword().equals(password)))
        {
            ContextHolder.setID(admin.getId());
            ContextHolder.setAccountType(AccountType.ADMIN);
            return admin;
        }
        else 
        {
            JOptionPane.showMessageDialog(null, "Invalid credentials, try again");
            return null;
        }
    }
}
