package com.example.EkoBikeRental.user.bike.rental.rent;

import com.example.EkoBikeRental.bank.BankAccount;
import com.example.EkoBikeRental.bank.BankAccountServices;
import com.example.EkoBikeRental.entities.Bike;
import com.example.EkoBikeRental.entities.BikeStatus;
import com.example.EkoBikeRental.entities.ParkingLot;
import com.example.EkoBikeRental.entities.Record;
import com.example.EkoBikeRental.entities.User;
import com.example.EkoBikeRental.entities.UserStatus;
import com.example.EkoBikeRental.repositories.BikeRepository;
import com.example.EkoBikeRental.repositories.ParkingLotRepository;
import com.example.EkoBikeRental.repositories.RecordRepository;
import com.example.EkoBikeRental.repositories.UserRepository;
import com.example.EkoBikeRental.util.ContextHolder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BankPaymentService {
    private BankAccountServices bankAccountServices;
    private UserRepository userRepository;
    private RecordRepository recordRepository;
    private BikeRepository bikeRepository;
    private ParkingLotRepository parkingLotRepository;

    @Autowired
    public BankPaymentService(BankAccountServices bankAccountServices, UserRepository userRepository, RecordRepository recordRepository, 
                            BikeRepository bikeRepository, ParkingLotRepository parkingLotRepository){
        this.bankAccountServices = bankAccountServices;
        this.userRepository = userRepository;
        this.recordRepository = recordRepository;
        this.bikeRepository = bikeRepository;
        this.parkingLotRepository = parkingLotRepository;
    }

    public User getCurrentUser(){
        return this.userRepository.findById(ContextHolder.getID()).get();
    }

    public BankAccount checkLogin(String accountNum, String password) throws Exception{
        return bankAccountServices.login(accountNum, password);
    }

    public boolean checkBalance(double bankAccountBalance, double depositFee){
        return bankAccountBalance >= depositFee;
    }

    @Transactional
    public Record saveRecord(Record record) throws Exception{
        ParkingLot parkingLot = record.getBike().getParkingLot();
        parkingLot.setNumBike(parkingLot.getNumBike() - 1);
        this.parkingLotRepository.save(parkingLot);
        record.getBike().setParkingLot(null);
        this.updateBikeStatus(record.getBike(), BikeStatus.BUSY);
        this.updateUserStatus(record.getUser(), UserStatus.RENTING);
        return this.recordRepository.save(record);
    }

    @Transactional
    public Bike updateBikeStatus(Bike bike, BikeStatus bikeStatus){
        bike.setBikeStatus(bikeStatus);
        return this.bikeRepository.save(bike);
    }

    // This function is created by Tien for TEST-ONLY purpose
    // Optimize later if you want
    @Transactional
    public User updateUserStatus(User user, UserStatus userStatus){
        user.setStatus(userStatus);
        return this.userRepository.save(user);
    }

    @Transactional
    public void doTransaction(Record record, String accountNum, String password) throws Exception{
        updateBankAccount(accountNum, password, record.getBike().calculateDepositFee());
        saveRecord(record);
    }

    @Transactional
    public BankAccount updateBankAccount(String accountNum, String password, Double change) throws Exception{
        return this.bankAccountServices.decreaseBalance(accountNum, password, change);
    }
}
