package com.example.EkoBikeRental.admin.bike.service;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import org.springframework.stereotype.Service;

import com.example.EkoBikeRental.abstracts.BikeService;
import com.example.EkoBikeRental.repositories.BikeRepository;

import javafx.scene.control.DatePicker;

@Service
public class BikeCheckInfoService extends BikeService {

	public BikeCheckInfoService(BikeRepository bikeRepository) {
		super(bikeRepository);
	}

    
    public void checkValidLicense(String license) throws Exception {
    	try {
    		checkNotEmptyLicense(license);
    		checkLicenseExist(license);
    	} catch(Exception e) {
    		throw new Exception(e.getMessage());
    	}
    }
    
    public void checkNotEmptyLicense(String license) throws Exception {
    	if (license.isEmpty()) {
			throw new Exception("License plate can not be empty");
		}
    }
    
	public void checkLicenseExist(String license) throws Exception {
		if(bikeRepository.checkLicenseExist(license) != 0){
			throw new Exception("License " + license + " already exists in the database");
		}
	}
	
    private void checkPositive (String value, String field) throws Exception {
    	if(!value.isEmpty()) {
    		try {
    			Double.parseDouble(value);
    		} catch(Exception e) {
    			throw new Exception(field + " must be a real number");
    		}
    		if(Double.valueOf(value) <= 0) throw new Exception(field + " must be greater than 0");
    	}
    }

    public void checkValidWeight(String weight) throws Exception {
    	checkPositive(weight, "Weight");
    }
    
    public void checkValidCost(String cost) throws Exception {
    	checkPositive(cost, "Cost");
    }
    
    public void checkValidDate(String date) throws Exception {
		if (!date.isEmpty()) {
			try {
		    	DatePicker datePicker = new DatePicker();
				datePicker.getConverter().fromString(date);
			} catch (DateTimeParseException e) {
				throw new Exception("The date is invalid, correct format is MM/DD/YYYY");
			}
		}
	}
    
    public void checkNotFutureDate(String date) throws Exception {
    	if(date.isEmpty()) return;
    	DatePicker datePicker = new DatePicker();
		LocalDate inputDate = datePicker.getConverter().fromString(date);
    	if (inputDate.compareTo(LocalDate.now()) > 0) {
    		throw new Exception("Can not choose a future date");
    	}
    }
}
