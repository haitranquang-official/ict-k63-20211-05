package com.example.EkoBikeRental.repositories;

import com.example.EkoBikeRental.entities.User;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long>{
    User findOneByAccount(String account);
}
