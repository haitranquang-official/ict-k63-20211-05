package com.example.EkoBikeRental.feecalculator;

public class SpecialFeeCalculator implements IFeeCalculator{

    @Override
    public Double calculate(Integer minutes) {
        double base1 = 10, base2 = 30;
        if(minutes < base1) return (double) 0;
        if(minutes < base2) return 10000 * 1.5;
        return (10000 + Math.ceil((minutes - base2) / 15) * 3000) * 1.5;
    }
}
