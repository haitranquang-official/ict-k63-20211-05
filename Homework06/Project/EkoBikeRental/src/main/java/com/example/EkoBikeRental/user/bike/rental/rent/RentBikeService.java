package com.example.EkoBikeRental.user.bike.rental.rent;

import java.util.Optional;

import com.example.EkoBikeRental.entities.Bike;
import com.example.EkoBikeRental.entities.BikeStatus;
import com.example.EkoBikeRental.repositories.BikeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class RentBikeService{

	@Autowired
	private BikeRepository bikeRepository;

    public Bike findBikeByBikeID(Long bikeID) throws Exception{
		Optional<Bike> bike = bikeRepository.findById(bikeID);
		if(!bike.isPresent()) throw new Exception("Bike not found");
		else return bike.get();
	}

	public BikeStatus checBikeStatus(Bike bike){
		return bike.getBikeStatus();
	}


}

