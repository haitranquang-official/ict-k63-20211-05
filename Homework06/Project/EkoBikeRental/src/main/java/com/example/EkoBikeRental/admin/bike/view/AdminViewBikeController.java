package com.example.EkoBikeRental.admin.bike.view;

import java.util.function.Predicate;

import javax.swing.JOptionPane;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.EkoBikeRental.abstracts.BikeTableController;
import com.example.EkoBikeRental.admin.bike.create.AddNewBikeController;
import com.example.EkoBikeRental.admin.bike.delete.DeleteBikeController;
import com.example.EkoBikeRental.admin.bike.edit.EditBikeController;
import com.example.EkoBikeRental.admin.bike.service.BikeCheckInfoService;
import com.example.EkoBikeRental.admin.bike.service.BikeUpdatePLService;
import com.example.EkoBikeRental.admin.bike.service.BikeUpdateService;
import com.example.EkoBikeRental.entities.Bike;
import com.example.EkoBikeRental.entities.BikeStatus;
import com.example.EkoBikeRental.util.SwitchScreenUtil;

import javafx.beans.property.SimpleLongProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

@Component
public class AdminViewBikeController extends BikeTableController {
	
    @Autowired
    protected SwitchScreenUtil switchScreenUtil;
	
    @Autowired
	private BikeCheckInfoService adminBikeService;
    
	@Autowired
	EditBikeController editBikeController;
    
    @Autowired
	AddNewBikeController addNewBikeController;
    
    @Autowired
    DeleteBikeController deleteBikeController;
    
	@FXML
	private TableColumn<Bike, Long> colPid;
	
	@FXML
    private Button homeBtn;
	
	@FXML
    private Button addBtn;
	
	@FXML
    private Button delBtn;
	
	@FXML
    private Button editBtn;
	
	@FXML
    private TextField tfSearch;
	
	@FXML
    private RadioButton searchById;

    @FXML
    private RadioButton searchByAll;
	
    @FXML
    void homeBtnHandler(ActionEvent event) throws Exception {
    	switchScreenUtil.switchScene(switchScreenUtil.ADMIN_MAIN_PAGE, event);
    }
    
	@FXML
	public void initialize() {
		colPid.setCellValueFactory(data -> new SimpleLongProperty(data.getValue().getParkingLot().getId()).asObject());
		ObservableList<Bike> observableList = FXCollections.observableArrayList(adminBikeService.findAll());
		initBikeTable(observableList);
		tblBike.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
    		updateButtonBar(newValue);
    	});
		showFilter(observableList);
    }
	
    private void setPredicate(FilteredList<Bike> filteredList, String tf) {
	    filteredList.setPredicate((Predicate<? super Bike>) bike ->{
	    	if (tf == null || tf.isEmpty()) return true;

	    	String lowerCaseFilter = tf.toLowerCase();
			
	    	if (bike.toString().toLowerCase().contains(lowerCaseFilter) && searchByAll.isSelected())
	    		return true;
	    	else if (bike.getId().toString().toLowerCase().contains(lowerCaseFilter) && searchById.isSelected())
	    		return true;
	    	
	    	return false;
	    });
	}
	
    public void showFilter(ObservableList<Bike> observableList) {
    	FilteredList<Bike> filteredList = new FilteredList<>(observableList);
    	tfSearch.textProperty().addListener((observableValue, oldValue, newValue) -> {
    		setPredicate(filteredList, newValue);
    	});
    	searchByAll.selectedProperty().addListener((observableValue, oldValue, newValue) -> {
    		String tf = tfSearch.getText();
    		setPredicate(filteredList, tf);
    	});
    	SortedList<Bike> sortedData = new SortedList<>(filteredList);
    	sortedData.comparatorProperty().bind(tblBike.comparatorProperty());
    	tblBike.setItems(sortedData);
    }
	
	protected void updateButtonBar(Bike bike) {
		if(bike == null || bike.getBikeStatus() == BikeStatus.BUSY) {
			delBtn.setDisable(true);
    		editBtn.setDisable(true);
    	}
    	else {
    		delBtn.setDisable(false);
    		editBtn.setDisable(false);
    	}
		
	}


	@FXML
    void editBtnHandler(ActionEvent event) throws Exception {
		Bike bike = tblBike.getSelectionModel().getSelectedItem();
		if (bike != null) {
			switchScreenUtil.switchAdminEditBike(event, editBikeController, bike);
		}
    }

    @FXML
    void delBtnHandler(ActionEvent event) {
    	deleteBikeController.deleteBike(tblBike.getSelectionModel().getSelectedItem(), this);
    }
    
    @FXML
    void addBtnHandler(ActionEvent event) throws Exception {
    	switchScreenUtil.switchAddNewBike(event, addNewBikeController);
    }

}
