package com.example.EkoBikeRental.user.parkinglot;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.EkoBikeRental.abstracts.ViewParkingLotController;
import com.example.EkoBikeRental.entities.Bike;
import com.example.EkoBikeRental.entities.BikeStatus;
import com.example.EkoBikeRental.entities.ParkingLot;
import com.example.EkoBikeRental.entities.UserStatus;
import com.example.EkoBikeRental.util.SwitchScreenUtil;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

@Component
public class UserViewParkingLotController extends ViewParkingLotController{
	
    @Autowired
    private SwitchScreenUtil switchScreenUtil;
	
    @Autowired
    private UserParkingLotService userParkingLotService;
	
    @FXML
    private Button btnRentBike;
    
    public void initialize(ParkingLot pl) {
    	initViewParkingLot(pl);
    	updateButtonBar();
    }
    
    protected void updateButtonBar() {
    	tblBike.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
        	if(newValue!=null && userParkingLotService.getUserStatus() == UserStatus.NOT_RENTING && tblBike.getSelectionModel().getSelectedItem().getBikeStatus()== BikeStatus.FREE) {
        		btnRentBike.setDisable(false);
        	} else btnRentBike.setDisable(true);
    	});
    }

	@FXML
    private void btnRentBikePressed(ActionEvent event) throws IOException {
    	Bike bike = tblBike.getSelectionModel().getSelectedItem();
    	switchScreenUtil.switchViewToRentBikeScene(event, bike.getId());
    }
    
    @FXML
	protected void btnBackPressed(ActionEvent event) throws IOException {
    	switchScreenUtil.switchScene(switchScreenUtil.USER_SEARCH_PARKING_LOT_PAGE, event);
    }

}
