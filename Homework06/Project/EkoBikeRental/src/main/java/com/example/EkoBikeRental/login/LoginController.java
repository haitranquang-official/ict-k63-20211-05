package com.example.EkoBikeRental.login;

import java.net.URL;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import com.example.EkoBikeRental.admin.parkinglot.create.CreateParkingLotController;
import com.example.EkoBikeRental.entities.Admin;
import com.example.EkoBikeRental.entities.User;
import com.example.EkoBikeRental.util.SwitchScreenUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;

@Component
public class LoginController implements Initializable{

    @Autowired
    private LoginService loginService;

    @Autowired
    private SwitchScreenUtil switchScreenUtil;

    @FXML
    private ToggleGroup AccountType;

    @FXML
    private RadioButton userButton;

    @FXML
    private RadioButton adminButton;

    @FXML
    private TextField accountTextField;

    @FXML
    private Button loginButton;

    @FXML
    private PasswordField passwordField;

    public void setLoginService(LoginService loginService)
    {
        this.loginService = loginService;
    }

    @FXML
    void loginButtonHandler(ActionEvent actionEvent) throws Exception {
        String account = accountTextField.getText();
        String password = passwordField.getText();
        if (AccountType.getSelectedToggle().equals(adminButton))
        {
            Admin admin = this.loginService.correctAdminLogin(account, password);
            if (admin!=null)
            {
                switchScreenUtil.switchScene(switchScreenUtil.ADMIN_MAIN_PAGE, actionEvent);
            }
        }
        if (AccountType.getSelectedToggle().equals(userButton))
        {
            User user = this.loginService.correctUserLogin(account, password);
            if (user!=null)
            {
                switchScreenUtil.switchScene(switchScreenUtil.MAIN_SCREEN, actionEvent);
            }
        }
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        userButton.setSelected(true);
        
    }

}
