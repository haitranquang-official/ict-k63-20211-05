package com.example.EkoBikeRental.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.example.EkoBikeRental.depositcalculator.NormalBikeDeposit;
import com.example.EkoBikeRental.feecalculator.IFeeCalculator;
import com.example.EkoBikeRental.feecalculator.NormalFeeCalculator;

import org.springframework.beans.factory.annotation.Autowired;

@Entity

@DiscriminatorValue("NORMAL_BIKE")
public class NormalBike extends  Bike{

    private transient NormalFeeCalculator normalFeeCalculator = new NormalFeeCalculator();
    
    private transient NormalBikeDeposit normalBikeDeposit = new NormalBikeDeposit();
    
    public NormalBike(){
        this.setType(BikeType.NORMAL_BIKE);
        this.setIFeeCalculator(normalFeeCalculator);
        this.setIDepositCalculator(normalBikeDeposit);
    }

    public Double calculateRentalFee(Integer minutes){
        return this.normalFeeCalculator.calculate(minutes);
    }

    @Override
    public Double calculateDepositFee(){
        return this.normalBikeDeposit.getDepositFee();
    }
}
