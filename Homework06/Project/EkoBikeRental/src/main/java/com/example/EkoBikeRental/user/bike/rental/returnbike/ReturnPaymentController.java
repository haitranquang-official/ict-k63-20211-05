package com.example.EkoBikeRental.user.bike.rental.returnbike;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import com.example.EkoBikeRental.bank.BankAccount;
import com.example.EkoBikeRental.entities.ParkingLot;
import com.example.EkoBikeRental.entities.Record;
import com.example.EkoBikeRental.entities.UserStatus;
import com.example.EkoBikeRental.repositories.ParkingLotRepository;
import com.example.EkoBikeRental.util.ContextHolder;
import com.example.EkoBikeRental.util.SwitchScreenUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

@Component
public class ReturnPaymentController implements Initializable {
    @Autowired
    private ReturnPaymentService returnPaymentService;

    @Autowired
    private SwitchScreenUtil switchScreenUtil;

    @Autowired
    private ParkingLotRepository parkingLotRepository;

    @FXML
    private TextField TotalFee;

    @FXML
    private Button ConfirmButton;

    @FXML
    private TextField AccountNum;

    @FXML
    private PasswordField Password;

    private Record record = new Record();

    private ParkingLot parkingLot = null;

    private BankAccount bankAccount = null;

    private Double rentalFee = null;

    private ActionEvent originEvent;

    public ReturnPaymentController() {
    };

    @FXML
    void confirmButtonHandler(ActionEvent event) {
        String accountNum = AccountNum.getText();
        String password = Password.getText();

        try {
            bankAccount = returnPaymentService.authenticate(accountNum, password);
            returnPaymentService.update(record, accountNum, password, parkingLot);

            JOptionPane.showMessageDialog(null, "Return bike successfully \nSee you again 🚲");

            switchScreenUtil.switchScene(switchScreenUtil.MAIN_SCREEN, originEvent);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.close();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        AccountNum.setText(ContextHolder.accountNum);
        init();
    }

    @FXML
    void backButtonHandler(ActionEvent event) throws IOException {
    	switchScreenUtil.switchScene(switchScreenUtil.USER_SEARCH_PARKING_LOT_PAGE, event);
    }

    public void init() {

        Long userId = ContextHolder.ID;

        try {
            parkingLot = returnPaymentService.getReturnParkingLot();
            record = returnPaymentService.findLatestRecordByUserId(userId);
            Double rentalFee = returnPaymentService.getRentalFee(record);
            TotalFee.setText("TOTAL: " + rentalFee + " VND");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
            return;
        }
    }

    public void initEvent(ActionEvent event) {
        this.originEvent = event;
    }
}
