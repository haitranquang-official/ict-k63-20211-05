package com.example.EkoBikeRental.admin.bike.service;

import org.springframework.stereotype.Service;

import com.example.EkoBikeRental.abstracts.ParkingLotService;
import com.example.EkoBikeRental.entities.ParkingLot;
import com.example.EkoBikeRental.repositories.ParkingLotRepository;

@Service
public class BikeCheckPLService extends ParkingLotService {
	
	public BikeCheckPLService(ParkingLotRepository parkingLotRepository) {
		super(parkingLotRepository);
	}

	public void checkNotNull (ParkingLot parkingLot) throws Exception {
		if (parkingLot == null) throw new Exception ("The parking lot can not be empty");
	}
	
	public void checkFullSlot (ParkingLot parkingLot) throws Exception {
		if (parkingLot.getNumBike() == parkingLot.getNumberOfSlots()) {
    		throw new Exception ("The parking lot is already full");
    	}
	}
	
    public void checkCanAddABike (ParkingLot parkingLot) throws Exception {
    	try {
    		checkNotNull(parkingLot);
    		checkFullSlot(parkingLot);
    	} catch(Exception e) {
    		throw new Exception(e.getMessage());
    	}
    }
}
