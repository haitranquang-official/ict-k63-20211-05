package com.example.EkoBikeRental.user;

import java.io.IOException;
import java.net.URL;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.ResourceBundle;

import com.example.EkoBikeRental.entities.Bike;
import com.example.EkoBikeRental.entities.Record;
import com.example.EkoBikeRental.entities.User;
import com.example.EkoBikeRental.entities.UserStatus;
import com.example.EkoBikeRental.repositories.RecordRepository;
import com.example.EkoBikeRental.repositories.UserRepository;
import com.example.EkoBikeRental.util.ContextHolder;
import com.example.EkoBikeRental.util.SwitchScreenUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

@Component
public class UserMainPageController implements Initializable {

    @Autowired
    private SwitchScreenUtil switchScreenUtil;

    @Autowired
    private RecordRepository recordRepository;

    @Autowired
    private UserRepository userRepository;

    @FXML
    private SplitPane BikeInfoPane;

    @FXML
    private VBox IntroPane;

    @FXML
    private TextField BikeTypeValue;

    @FXML
    private TextField Producer;

    @FXML
    private Button ReturnBikeButton;

    @FXML
    private Button ViewParkingLotButton;

    @FXML
    private Text UserID;

    @FXML
    private TextField BikeValue;

    @FXML
    private TextField StartTime;

    @FXML
    private TextField ElapsedTime;

    @FXML
    private TextField LicencePlate;

    @FXML
    private Button StartRentingButton;

    @FXML
    private Text BikeCode;

    @FXML
    void returnBikeButtonHandler(ActionEvent event) throws IOException {
        switchScreenUtil.switchToReturnBikeScreen(event);
    }

    @FXML
    void logoutButtonHandler(ActionEvent event) throws IOException {
        ContextHolder.logOut();
        this.switchScreenUtil.switchScene(switchScreenUtil.LOGIN_PAGE, event);
    }

    @FXML
    void startRentingButtonHandler(ActionEvent event) throws IOException {
        switchScreenUtil.switchScene(switchScreenUtil.RENT_BIKE_PAGE, event);
    }

    @FXML
    void viewParkingLotButtonHandler(ActionEvent event) throws IOException{
        switchScreenUtil.switchScene(switchScreenUtil.USER_SEARCH_PARKING_LOT_PAGE, event);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        User user = userRepository.getById(ContextHolder.ID);
        UserID.setText("Username: " + user.getAccount());
        switch (user.getStatus()) {
            case RENTING:
                initReturn();
                break;
        
            default:
                initRent();
                break;
        }
    }

    public void initRent(){
        BikeInfoPane.setVisible(false);
        ReturnBikeButton.setVisible(false);
        IntroPane.setVisible(true);
    }

    public void initReturn(){
        BikeInfoPane.setVisible(true);
        ReturnBikeButton.setVisible(true);
        ViewParkingLotButton.setVisible(false);
        IntroPane.setVisible(false);
        List<Record> records = recordRepository.findByUserId(ContextHolder.ID);
        Record record = records.get(records.size() - 1);

        StartTime.setText(record.getStartTime().toString());
        ElapsedTime.setText(Duration.between(record.getStartTime(), LocalDateTime.now()).toMinutes() + " mins");

        Bike bike = record.getBike();
        BikeCode.setText("BIKE CODE: " + bike.getId());
        LicencePlate.setText(bike.getLicensePlate());
        BikeTypeValue.setText(bike.getType().toString());
        Producer.setText(bike.getProducer());
        BikeValue.setText(bike.getCost().toString());
    }

}
