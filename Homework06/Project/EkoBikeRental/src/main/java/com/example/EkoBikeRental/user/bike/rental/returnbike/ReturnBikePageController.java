package com.example.EkoBikeRental.user.bike.rental.returnbike;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import com.example.EkoBikeRental.entities.Bike;
import com.example.EkoBikeRental.entities.Record;
import com.example.EkoBikeRental.util.ContextHolder;
import com.example.EkoBikeRental.util.SwitchScreenUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

@Component
public class ReturnBikePageController implements Initializable {

    @Autowired
    private ReturnBikeService returnBikeService;

    @Autowired
    private SwitchScreenUtil switchScreenUtil;

    @FXML
    private Text BikeCode;

    @FXML
    private Text BikeName;

    @FXML
    private TextField BikeTypeValue;

    @FXML
    private TextField StartTime;

    @FXML
    private TextField RentalFee;

    @FXML
    private TextField ElapsedTime;

    @FXML
    private Button TotalFee;

    @FXML
    private TextField LateFee;

    @FXML
    private Button ContinueButton;

    @FXML
    private Button HomeButton;

    private Bike bike;

    private Record record = new Record();

    public ReturnBikePageController() {
    };

    @FXML
    void continueButtonHandler(ActionEvent event) throws IOException {
    	switchScreenUtil.switchScene(switchScreenUtil.USER_SEARCH_PARKING_LOT_PAGE, event);
        // switchScreenUtil.popUpReturnPayment();
    }

    @FXML
    void homeButtonHandler(ActionEvent event) throws IOException {
        switchScreenUtil.switchScene(switchScreenUtil.MAIN_SCREEN, event);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    public void init() {
        try {
            record = returnBikeService.findLatestRecordByUserId(ContextHolder.ID);
            bike = returnBikeService.findBikeByBikeID(record.getBike().getId());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
            return;
        }
        
        BikeCode.setText("BIKE CODE: " + bike.getId());
        BikeName.setText(String.valueOf(bike.getName()));
        BikeTypeValue.setText(String.valueOf(bike.getType()));

        StartTime.setText(String.valueOf(record.getStartTime()));
        Integer elapsedTime = returnBikeService.getCurrentRentalTime(record);
        ElapsedTime.setText(String.valueOf(elapsedTime) + " mins");
        Double rentalFee = returnBikeService.calculateRentalFee(record);
        RentalFee.setText(String.valueOf(rentalFee));
        TotalFee.setText("TOTAL: " + rentalFee + " VND");

    }
}
