package com.example.EkoBikeRental.user.bike.rental.returnbike;

import java.time.LocalDateTime;
import java.util.List;

import com.example.EkoBikeRental.bank.BankAccount;
import com.example.EkoBikeRental.bank.BankAccountServices;
import com.example.EkoBikeRental.entities.Bike;
import com.example.EkoBikeRental.entities.BikeStatus;
import com.example.EkoBikeRental.entities.ParkingLot;
import com.example.EkoBikeRental.entities.Record;
import com.example.EkoBikeRental.entities.User;
import com.example.EkoBikeRental.entities.UserStatus;
import com.example.EkoBikeRental.repositories.BikeRepository;
import com.example.EkoBikeRental.repositories.ParkingLotRepository;
import com.example.EkoBikeRental.repositories.RecordRepository;
import com.example.EkoBikeRental.repositories.UserRepository;
import com.example.EkoBikeRental.util.ContextHolder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ReturnPaymentService {
    private RecordRepository recordRepository;
    private BankAccountServices bankAccountServices;
    private BikeRepository bikeRepository;
    private UserRepository userRepository;
    private ParkingLotRepository parkingLotRepository;

    @Autowired
    public ReturnPaymentService(RecordRepository recordRepository, BankAccountServices bankAccountServices,
                                BikeRepository bikeRepository, UserRepository userRepository, ParkingLotRepository parkingLotRepository) {
        this.recordRepository = recordRepository;
        this.bankAccountServices = bankAccountServices;
        this.bikeRepository = bikeRepository;
        this.userRepository = userRepository;
        this.parkingLotRepository = parkingLotRepository;
    };

    public Record findLatestRecordByUserId(Long userId) {
        List<Record> records = recordRepository.findByUserId(userId);
        Record record = records.get(records.size() - 1);
        return record;
    }

    
    public ParkingLot getReturnParkingLot(){
        return parkingLotRepository.getById(ContextHolder.returnParkingLotId);
    }

    public BankAccount authenticate(String accountNum, String password) throws Exception {
        return bankAccountServices.login(accountNum, password);
    }

    @Transactional
    public void update(Record record, String accountNum, String password, ParkingLot parkingLot)
            throws Exception {
        Bike bike = record.getBike();
        updateBike(bike, parkingLot);
        updateUserStatus(record.getUser());
        updateBankAccount(accountNum, password, bike.calculateDepositFee(), getRentalFee(record));
        updateParkingLot(bike.getParkingLot());
        updateRecord(record);
    }

    @Transactional
    public Record updateRecord(Record record){
        record.setEndTime(LocalDateTime.now());
        record.setCost(getRentalFee(record));
        return recordRepository.save(record);
    }

    @Transactional
    public Bike updateBike(Bike bike, ParkingLot returnParkingLot) {
        bike.setBikeStatus(BikeStatus.FREE);
        bike.setParkingLot(returnParkingLot);
        return bikeRepository.save(bike);
    }

    @Transactional
    public ParkingLot updateParkingLot(ParkingLot parkingLot){
        if(parkingLot.getNumBike() == parkingLot.getNumberOfSlots())
            throw new IndexOutOfBoundsException("This parking lot is already full");
        parkingLot.setNumBike(parkingLot.getNumBike() + 1);
        return parkingLotRepository.save(parkingLot);
    }

    @Transactional
    public User updateUserStatus(User user) {
        user.setStatus(UserStatus.NOT_RENTING);
        return userRepository.save(user);
    }

    @Transactional
    public void updateBankAccount(String accountNum, String password, Double depositFee, Double rentalFee)
            throws Exception {
        bankAccountServices.increaseBalance(accountNum, password, depositFee);
        bankAccountServices.decreaseBalance(accountNum, password, rentalFee);
    }

    public Double getRentalFee(Record record) {
        Integer elapsedTime = (int) java.time.Duration.between(record.getStartTime(), LocalDateTime.now())
                .toMinutes();
        Double rentalFee = record.getBike().calculateRentalFee(elapsedTime);
        return rentalFee;
    }
}
