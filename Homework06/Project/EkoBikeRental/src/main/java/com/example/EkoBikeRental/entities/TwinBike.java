package com.example.EkoBikeRental.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.example.EkoBikeRental.depositcalculator.IDepositCalculator;
import com.example.EkoBikeRental.depositcalculator.TwinBikeDeposit;
import com.example.EkoBikeRental.feecalculator.SpecialFeeCalculator;

import org.springframework.beans.factory.annotation.Autowired;

@Entity
@DiscriminatorValue("TWIN_BIKE")
public class TwinBike extends Bike{

    private transient SpecialFeeCalculator specialFeeCalculator = new SpecialFeeCalculator();
    private transient TwinBikeDeposit twinBikeDeposit = new TwinBikeDeposit();

    public TwinBike(){
        this.setType(BikeType.TWIN_BIKE);
        this.setIFeeCalculator(specialFeeCalculator);
        this.setIDepositCalculator(twinBikeDeposit);
    }

    public Double calculateRentalFee(Integer minutes){
        return this.specialFeeCalculator.calculate(minutes);
    }

    @Override
    public Double calculateDepositFee(){
        return this.twinBikeDeposit.getDepositFee();
    }
}
