package com.example.EkoBikeRental.entities;

public enum BikeStatus {
    FREE,
    BUSY,
    NOT_IN_USE
}
