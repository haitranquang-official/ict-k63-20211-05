package com.example.EkoBikeRental.abstracts;

import java.io.IOException;

import com.example.EkoBikeRental.entities.ParkingLot;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public abstract class ViewParkingLotController extends BikeTableController {

    @FXML
    protected Label numEBike;

    @FXML
    protected Label numDock;

    @FXML
    protected Label pLName;

    @FXML
    protected Label numBike;

    @FXML
    protected Label pLAddress;

    @FXML
    protected Label numTwinBike;
    
    protected void initViewParkingLot(ParkingLot pl) {
    	initBikeTable(FXCollections.observableArrayList(pl.getBikes()));
    	showLabelText(pl);
    }
    
    private void showLabelText(ParkingLot pl){
    	pLName.setText(pl.getName());
    	pLAddress.setText(pl.getAddress());
    	numBike.setText(pl.normalBikeProperty().getValue().toString());
    	numEBike.setText(pl.eBikeProperty().getValue().toString());
    	numTwinBike.setText(pl.twinBikeProperty().getValue().toString());
    	numDock.setText(pl.emptyDockProperty().getValue().toString());
    }
    
    protected abstract void updateButtonBar();
    
    protected abstract void btnBackPressed(ActionEvent event) throws IOException;
}
