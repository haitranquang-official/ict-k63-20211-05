package com.example.EkoBikeRental.abstracts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.EkoBikeRental.entities.Bike;
import com.example.EkoBikeRental.repositories.BikeRepository;

public class BikeService {
	protected BikeRepository bikeRepository;
	
	@Autowired
    public BikeService(BikeRepository bikeRepository) {
        this.bikeRepository = bikeRepository;
    }
	
	public List<Bike> findAll(){
        return this.bikeRepository.findAll();
    }
}
