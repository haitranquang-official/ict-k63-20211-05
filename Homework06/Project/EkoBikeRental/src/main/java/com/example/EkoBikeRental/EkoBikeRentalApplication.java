package com.example.EkoBikeRental;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import javafx.application.Application;

@SpringBootApplication
public class EkoBikeRentalApplication {

	public static void main(String[] args) {
		Application.launch(JavaFXApplication.class, args);
	}
}

