package com.example.EkoBikeRental.entities;

import java.time.LocalDateTime;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.example.EkoBikeRental.depositcalculator.EBikeDeposit;
import com.example.EkoBikeRental.feecalculator.SpecialFeeCalculator;

@Entity
@DiscriminatorValue("ELECTRICAL_BIKE")
public class EBike extends Bike{

    private  Integer batteryPercentage = 100;

    private transient SpecialFeeCalculator specialFeeCalculator = new SpecialFeeCalculator();
    
    private transient EBikeDeposit eBikeDeposit = new EBikeDeposit();

    public EBike() {
        this.setType(BikeType.ELECTRICAL_BIKE);
        this.setIFeeCalculator(specialFeeCalculator);
        this.setIDepositCalculator(eBikeDeposit);
    }

    // public EBike(String name, Double weight, String licensePlate, LocalDateTime manufacturingDate, String status, Integer batteryPercentage) {
    //     super(name, weight, licensePlate, manufacturingDate, status);
    //     this.batteryPercentage = batteryPercentage;
    //     this.setType(BikeType.ELECTRICAL_BIKE);
    //     this.setIFeeCalculator(specialFeeCalculator);
    //     this.setIDepositCalculator(eBikeDeposit);
    // }

    public EBike(String name) {
        super(name);
        this.setIFeeCalculator(specialFeeCalculator);
        this.setIDepositCalculator(eBikeDeposit);
    }

    public Integer getBatteryPercentage() {
        return batteryPercentage;
    }

    public void setBatteryPercentage(Integer batteryPercentage) {
        this.batteryPercentage = batteryPercentage;
    }

    public Double calculateRentalFee(Integer minutes){
        return this.specialFeeCalculator.calculate(minutes);
    }

    @Override
    public Double calculateDepositFee(){
        return this.eBikeDeposit.getDepositFee();
    }

}
