package com.example.EkoBikeRental.abstracts;

import java.util.function.Predicate;

import org.springframework.stereotype.Service;

import com.example.EkoBikeRental.entities.ParkingLot;
import com.example.EkoBikeRental.repositories.ParkingLotRepository;

import javafx.collections.transformation.FilteredList;

@Service
public class SearchParkingLotService extends ParkingLotService {

	public SearchParkingLotService(ParkingLotRepository parkingLotRepository) {
		super(parkingLotRepository);
	}
}
