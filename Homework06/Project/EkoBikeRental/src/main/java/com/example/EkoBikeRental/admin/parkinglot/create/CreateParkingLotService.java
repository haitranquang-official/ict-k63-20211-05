package com.example.EkoBikeRental.admin.parkinglot.create;

import java.io.IOException;
import java.util.List;

import javax.swing.JFrame;
import javax.transaction.Transactional;

import com.example.EkoBikeRental.abstracts.ParkingLotService;
import com.example.EkoBikeRental.entities.ParkingLot;
import com.example.EkoBikeRental.repositories.ParkingLotRepository;
import com.example.EkoBikeRental.util.AccountType;
import com.example.EkoBikeRental.util.ContextHolder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

@Service
public class CreateParkingLotService extends ParkingLotService{

   
    public CreateParkingLotService(ParkingLotRepository parkingLotRepository) {
        super(parkingLotRepository);
        //TODO Auto-generated constructor stub
    }

    @Transactional
    public void addParkingLot(ParkingLot parkingLot) throws Exception
    {
        if (!ContextHolder.getAccountType().equals(AccountType.ADMIN))
        {
            throw new Exception("Only admins are allowed to create new bike");
        }
        if (findParkingLotByName(parkingLot.getName())!=null)
        {
            throw new Exception("Another parking lot with the same name has already existed");
        }
        if (parkingLot.getNumberOfSlots() < 0)
        {
            throw new Exception("Invalid capacity");
        }
        this.parkingLotRepository.save(parkingLot);
    }

    public ParkingLot findParkingLotByName(String name)
    {
        return this.parkingLotRepository.findParkingLotByName(name);
    }

    
}
