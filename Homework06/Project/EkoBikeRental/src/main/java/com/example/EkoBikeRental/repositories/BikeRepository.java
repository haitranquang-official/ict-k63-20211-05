package com.example.EkoBikeRental.repositories;
import com.example.EkoBikeRental.entities.Bike;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface BikeRepository extends JpaRepository<Bike, Long>{
	@Query(value="select count(*) from bike where license_plate =:license", nativeQuery=true)
	int checkLicenseExist(@Param("license") String license);
	
	@Query(value="delete from bike where bike_id =:id", nativeQuery=true)
	void deleteBike(@Param("id") Long id);
}
