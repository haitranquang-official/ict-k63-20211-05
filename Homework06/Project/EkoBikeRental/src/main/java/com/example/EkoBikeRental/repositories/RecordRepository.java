package com.example.EkoBikeRental.repositories;

import java.util.List;

import com.example.EkoBikeRental.entities.Record;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RecordRepository extends JpaRepository<Record, Long> {
    public List<Record> findByUserId(Long userId);
}
