package com.example.EkoBikeRental.admin.parkinglot;

import java.io.IOException;

import com.example.EkoBikeRental.abstracts.SearchParkingLotController;
import com.example.EkoBikeRental.abstracts.ViewParkingLotController;
import com.example.EkoBikeRental.entities.Bike;
import com.example.EkoBikeRental.entities.ParkingLot;
import com.example.EkoBikeRental.entities.UserStatus;
import com.example.EkoBikeRental.util.SwitchScreenUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

@Component
public class AdminSearchParkingLotController extends SearchParkingLotController{
	
    @Autowired
    protected SwitchScreenUtil switchScreenUtil;

    @FXML
    private Button createButton;

    @FXML
    private Button editButton;

    @FXML
    private Button updateButton;
    
    @FXML
    private void initialize() {
    	initSearchParkingLot();
    	updateButtonBar();
    }

    @FXML
    private void createButtonHandler(ActionEvent event) throws IOException {
        this.switchScreenUtil.switchScene(switchScreenUtil.CREATE_PARKING_LOT_PAGE, event);
    }
    
    @FXML
	protected void btnBackPressed(ActionEvent event) throws IOException {
        // TODO Auto-generated method stub
        this.switchScreenUtil.switchScene(switchScreenUtil.ADMIN_MAIN_PAGE, event);
    }

    protected void updateButtonBar() {
    	tblParkingLot.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
    		if(newValue!=null) {
    			editButton.setDisable(false);
    			updateButton.setDisable(false);
    		}
    		else{
    			editButton.setDisable(true);
    			updateButton.setDisable(true);
    		}
    	});
        
    }
}
