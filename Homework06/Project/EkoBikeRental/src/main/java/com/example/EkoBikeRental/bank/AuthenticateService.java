package com.example.EkoBikeRental.bank;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class AuthenticateService {
    private BankAccountRepository bankAccountRepository;

    @Autowired
    public AuthenticateService(BankAccountRepository bankAccountRepository) {
        this.bankAccountRepository = bankAccountRepository;
    }

    public BankAccount authenticate(String accountNum, String password) throws Exception {
        Optional<BankAccount> bankAccount = this.bankAccountRepository.findByAccountNum(accountNum);
        if (!bankAccount.isPresent())
            throw new Exception("Invalid account number");
        if (bankAccount.get().getPassword().equals(password))
            return bankAccount.get();
        throw new Exception("Wrong password");
    }

}
