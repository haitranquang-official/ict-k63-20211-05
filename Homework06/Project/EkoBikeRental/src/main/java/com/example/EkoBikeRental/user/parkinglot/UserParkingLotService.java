package com.example.EkoBikeRental.user.parkinglot;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.EkoBikeRental.entities.UserStatus;
import com.example.EkoBikeRental.repositories.UserRepository;
import com.example.EkoBikeRental.util.ContextHolder;
import com.example.EkoBikeRental.util.SwitchScreenUtil;

import javafx.event.ActionEvent;

@Service
public class UserParkingLotService {
   
    private UserRepository userRepository;
    
    @Autowired
    public UserParkingLotService(UserRepository userRepository){
        this.userRepository = userRepository;
    }
    
    public UserStatus getUserStatus() {
    	return userRepository.getById(ContextHolder.ID).getStatus();
    }

}
