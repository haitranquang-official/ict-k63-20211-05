package com.example.EkoBikeRental.admin;

import java.io.IOException;

import com.example.EkoBikeRental.repositories.AdminRepository;
import com.example.EkoBikeRental.repositories.UserRepository;
import com.example.EkoBikeRental.util.ContextHolder;
import com.example.EkoBikeRental.util.SwitchScreenUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

@Component
public class EcoBikeManagerController {

    @Autowired
    private SwitchScreenUtil switchScreenUtil;
    
    @Autowired
    private AdminRepository adminRepository;

    @FXML
    private Button createBikeButton;

    @FXML
    private Button createParkingLotButton;

    @FXML
    private Button signOutButton;
    
    @FXML
    private Label accountLbl;
    
    @FXML
    void loadCreateBikeScene(ActionEvent event) throws IOException {
        this.switchScreenUtil.switchScene(switchScreenUtil.ADMIN_VIEW_BIKE_PAGE, event);
    }

    @FXML
    void loadParkingLotScene(ActionEvent event) throws IOException{
        this.switchScreenUtil.switchScene(switchScreenUtil.ADMIN_SEARCH_PARKING_LOT_PAGE, event);
    }

    @FXML
    void signOutHandler(ActionEvent event) throws IOException {
        ContextHolder.logOut();
        this.switchScreenUtil.switchScene(switchScreenUtil.LOGIN_PAGE, event);
    }
    
    @FXML
    private void initialize() {
    	String account = adminRepository.getById(ContextHolder.getID()).getAccount();
    	accountLbl.setText("Admin Account: " + account);
    }

}
