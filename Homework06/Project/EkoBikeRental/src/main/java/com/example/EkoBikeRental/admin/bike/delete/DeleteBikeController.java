package com.example.EkoBikeRental.admin.bike.delete;

import javax.swing.JOptionPane;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.EkoBikeRental.admin.bike.service.BikeUpdatePLService;
import com.example.EkoBikeRental.admin.bike.service.BikeUpdateService;
import com.example.EkoBikeRental.admin.bike.view.AdminViewBikeController;
import com.example.EkoBikeRental.entities.Bike;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

@Component
public class DeleteBikeController {

	@Autowired
	private BikeUpdatePLService updateParkingLotService;
	
	@Autowired 
	private BikeUpdateService adminBikeService;
	
	public void deleteBike(Bike bike, AdminViewBikeController adminViewBikeController) {
		int opt = JOptionPane.showConfirmDialog(null, "Are you sure to delete this bike?", "Question", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
		if (opt == JOptionPane.YES_OPTION) {
    		try {
    			updateParkingLotService.removeABikeFromPl(bike.getParkingLot());
    			adminBikeService.deleteBike(bike);
    	    	ObservableList<Bike> listBike = FXCollections.observableArrayList(adminBikeService.findAll());
    	    	adminViewBikeController.showFilter(listBike);
        		JOptionPane.showMessageDialog(null, "Successfully deleted a bike");
        		} catch(Exception e) {
                e.printStackTrace();
            }	
    	}     
	}
}
