package com.example.EkoBikeRental.bank;

import java.util.Optional;

import com.example.EkoBikeRental.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BankAccountServices {
    private BankAccountRepository bankAccountRepository;
    private AuthenticateService authenticateService;

    @Autowired
    public BankAccountServices(BankAccountRepository bankAccountRepository, AuthenticateService authenticateService) {
        this.bankAccountRepository = bankAccountRepository;
        this.authenticateService = authenticateService;
    }

    public BankAccount increaseBalance(String accountNum, String password, Double balance) throws Exception {
        BankAccount bankAccount = authenticateService.authenticate(accountNum, password);
        bankAccount.setBalance(bankAccount.getBalance() + balance);
        bankAccountRepository.save(bankAccount);
        return bankAccount;
    }

    public BankAccount decreaseBalance(String accountNum, String password, Double balance) throws Exception {
        BankAccount bankAccount = authenticateService.authenticate(accountNum, password);
        Double currentBalance = bankAccount.getBalance();
        if (balance > currentBalance) {
            throw new Exception("Not enough balance");
        }
        bankAccount.setBalance(bankAccount.getBalance() - balance);
        bankAccountRepository.save(bankAccount);
        return bankAccount;
    }

    public BankAccount login(String accountNum, String password) throws Exception{
        return authenticateService.authenticate(accountNum, password);
    }
}
