package com.example.EkoBikeRental.admin.bike.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.EkoBikeRental.abstracts.ParkingLotService;
import com.example.EkoBikeRental.entities.ParkingLot;
import com.example.EkoBikeRental.repositories.ParkingLotRepository;

@Service
public class BikeUpdatePLService extends ParkingLotService {
	
	public BikeUpdatePLService(ParkingLotRepository parkingLotRepository) {
		super(parkingLotRepository);
	}

	@Transactional
	public void updateParkingLot(ParkingLot parkingLot) {
		parkingLotRepository.save(parkingLot);
	}
	
	@Transactional
	public void addABikeToParkingLot(ParkingLot parkingLot) {
		parkingLot.setNumBike(parkingLot.getNumBike()+1);
		updateParkingLot(parkingLot);
	}
    
    @Transactional
    public void removeABikeFromPl(ParkingLot parkingLot) {
    	parkingLot.setNumBike(parkingLot.getNumBike()-1);
    	updateParkingLot(parkingLot);
    }
    
}
