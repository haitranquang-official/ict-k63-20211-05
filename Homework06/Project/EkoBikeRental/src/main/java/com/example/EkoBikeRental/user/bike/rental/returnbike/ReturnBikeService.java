package com.example.EkoBikeRental.user.bike.rental.returnbike;

import java.time.LocalDateTime;
import java.util.List;

import com.example.EkoBikeRental.entities.Bike;
import com.example.EkoBikeRental.entities.BikeStatus;
import com.example.EkoBikeRental.entities.Record;
import com.example.EkoBikeRental.repositories.BikeRepository;
import com.example.EkoBikeRental.repositories.RecordRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReturnBikeService {

	private BikeRepository bikeRepository;
	private RecordRepository recordRepository;

	@Autowired
	public ReturnBikeService(BikeRepository bikeRepository, RecordRepository recordRepository) {
		this.bikeRepository = bikeRepository;
		this.recordRepository = recordRepository;
	};

	public Bike findBikeByBikeID(Long bikeID) throws Exception {
		Bike bike = bikeRepository.findById(bikeID).get();
		if (bike != null) {
			if (bike.getBikeStatus() != BikeStatus.BUSY) {
				throw new Exception("You are not renting this bike.");
			} else
				return bike;
		}
		throw new Exception("Bike not exists");
	}

	public Record findLatestRecordByUserId(Long userId){
		List<Record> records = recordRepository.findByUserId(userId);
		return records.get(records.size() - 1);
	}

	public Integer getCurrentRentalTime(Record record){
		return (int) java.time.Duration.between(record.getStartTime(), LocalDateTime.now()).toMinutes();
	}

	public Double calculateRentalFee(Record record){
		Integer elapsedTime = getCurrentRentalTime(record);
		Bike bike = record.getBike();
		return bike.calculateRentalFee(elapsedTime);
	}
}