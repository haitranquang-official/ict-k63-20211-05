package com.example.EkoBikeRental.repositories;

import java.util.List;

import com.example.EkoBikeRental.entities.ParkingLot;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ParkingLotRepository extends JpaRepository<ParkingLot, Long>{
    ParkingLot findParkingLotByName(String name);
    ParkingLot findParkingLotByAddress(String address);
}
