package com.example.EkoBikeRental.abstracts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.EkoBikeRental.entities.ParkingLot;
import com.example.EkoBikeRental.repositories.ParkingLotRepository;

public class ParkingLotService {
	
    protected ParkingLotRepository parkingLotRepository;
    @Autowired
    public ParkingLotService(ParkingLotRepository parkingLotRepository){
        this.parkingLotRepository = parkingLotRepository;
    }
    public List<ParkingLot> findAll(){
        return this.parkingLotRepository.findAll();
    }

}
