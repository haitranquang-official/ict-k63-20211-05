package com.example.EkoBikeRental.entities;

import javax.persistence.*;

import com.example.EkoBikeRental.feecalculator.IFeeCalculator;
import com.example.EkoBikeRental.depositcalculator.IDepositCalculator;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Table
@Entity
@Inheritance
@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING, name = "bike_type")
public abstract class Bike {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="bike_id")
    private Long id;

    private String name;
    @Enumerated(EnumType.STRING)
    private BikeType type;
    private Double weight;
    @Column(name = "license_plate")
    private String licensePlate;
    @Column(name = "manufacturing_date")
    private LocalDate manufacturingDate;
    private String producer;
    private Double cost;
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private BikeStatus bikeStatus = BikeStatus.FREE;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "parking_lot_id")
    @JsonBackReference(value = "parking_lot - bike")
    private ParkingLot parkingLot;
    @OneToMany(mappedBy = "bike")
    @JsonManagedReference(value = "bike - record")
    private List<Record> records;

    private transient IFeeCalculator iFeeCalculator;

    private transient IDepositCalculator iDepositCalculator;

    public Double calculateRentalFee(Integer minutes){
        return this.iFeeCalculator.calculate(minutes);
    }

    public Double calculateDepositFee(){
        return this.iDepositCalculator.getDepositFee();
    }
    
    public Bike(){
    }

    // public Bike(String name, BikeType type, Double weight, String licensePlate, LocalDate manufacturingDate, String producer, Double cost) {
    //     this.name = name;
    //     this.type = type;
    //     this.weight = weight;
    //     this.licensePlate = licensePlate;
    //     this.manufacturingDate = manufacturingDate;
    //     this.producer = producer;
    //     this.cost = cost;
    // }

    // public Bike(String name, Double weight, String licensePlate, LocalDateTime manufacturingDate, String status) {
    // }

    public Bike(String name) {
    }

    public ParkingLot getParkingLot() {
        return this.parkingLot;
    }

    public void setParkingLot(ParkingLot parkingLot) {
        this.parkingLot = parkingLot;
    }

    public IFeeCalculator getIFeeCalculator() {
        return this.iFeeCalculator;
    }

    public void setIFeeCalculator(IFeeCalculator iFeeCalculator) {
        this.iFeeCalculator = iFeeCalculator;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BikeType getType() {
        return type;
    }

    public void setType(BikeType type) {
        this.type = type;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public LocalDate getManufacturingDate() {
        return manufacturingDate;
    }

    public void setManufacturingDate(LocalDate localDate) {
        this.manufacturingDate = localDate;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public BikeStatus getBikeStatus() {
        return bikeStatus;
    }

    public void setBikeStatus(BikeStatus bikeStatus) {
        this.bikeStatus = bikeStatus;
    }

    public List<Record> getRecords() {
        return records;
    }

    public void setRecords(List<Record> records) {
        this.records = records;
    }

    public IDepositCalculator getIDepositCalculator() {
        return this.iDepositCalculator;
    }

    public void setIDepositCalculator(IDepositCalculator iDepositCalculator) {
        this.iDepositCalculator = iDepositCalculator;
    }

    @Override
    public String toString() {
        return "Bike [\n\tbikeStatus=" + bikeStatus + "\n\tcost=" + cost + "\n\tid=" + id + "\n\tlicensePlate=" + licensePlate
                + "\n\tmanufacturingDate=" + manufacturingDate + "\n\tname=" + name 
                + "\n\tproducer=" + producer + "\n\ttype=" + type + "\n\tweight=" + weight + "\n]";
    }
    
    
}
