package com.example.EkoBikeRental.abstracts;

import java.io.IOException;
import java.util.List;
import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.EkoBikeRental.admin.parkinglot.create.CreateParkingLotService;
import com.example.EkoBikeRental.entities.ParkingLot;
import com.example.EkoBikeRental.user.parkinglot.UserParkingLotService;
import com.example.EkoBikeRental.util.SwitchScreenUtil;

import javafx.collections.FXCollections;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;

public abstract class SearchParkingLotController extends ParkingLotTableController {

    @Autowired
    protected SearchParkingLotService searchParkingLotService;

    @FXML
    protected ToggleGroup searchParkingLot;
    
    @FXML
    protected RadioButton radioBtnSearchByAddress;
    
    @FXML
    protected RadioButton radioBtnSearchByName;

    @FXML
    protected TextField tfSearch;
    
    protected void initSearchParkingLot() {
    	List<ParkingLot> parkingLots = this.searchParkingLotService.findAll();
    	initParkingLotTable();
		showFilter(parkingLots);
    }
	
    private void showFilter(List<ParkingLot> parkingLots) {
    	FilteredList<ParkingLot> filteredList = new FilteredList<>(FXCollections.observableArrayList(parkingLots));
    	
    	tfSearch.textProperty().addListener((observableValue, oldValue, newValue) -> {
    		setPredicate(filteredList, newValue, radioBtnSearchByName.isSelected());
    	});
    	radioBtnSearchByName.selectedProperty().addListener((observableValue, oldValue, newValue) -> {
    		String tf = tfSearch.getText();
    		setPredicate(filteredList, tf, radioBtnSearchByName.isSelected());
    	});
    	SortedList<ParkingLot> sortedData = new SortedList<>(filteredList);
    	sortedData.comparatorProperty().bind(tblParkingLot.comparatorProperty());
    	tblParkingLot.setItems(sortedData);
    }
    
    public void setPredicate(FilteredList<ParkingLot> filteredList, String searchField, boolean searchByName) {
	    filteredList.setPredicate((Predicate<ParkingLot>) pl -> {
	    	if (searchField == null || searchField.isEmpty()) return true;

	    	String lowerCaseFilter = searchField.toLowerCase();
			
	    	if (pl.getName().toLowerCase().contains(lowerCaseFilter) && searchByName) {
	    		return true;
	    	} else if (pl.getAddress().toLowerCase().contains(lowerCaseFilter) && !searchByName) {
	    		return true;
	    	}
	    	
	    	return false;
	    });
	}
    
    protected abstract void updateButtonBar();
    
    protected abstract void btnBackPressed(ActionEvent event) throws IOException;

}
