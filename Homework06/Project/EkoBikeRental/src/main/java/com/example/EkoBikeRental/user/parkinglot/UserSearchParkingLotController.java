package com.example.EkoBikeRental.user.parkinglot;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.EkoBikeRental.abstracts.SearchParkingLotController;
import com.example.EkoBikeRental.entities.ParkingLot;
import com.example.EkoBikeRental.entities.UserStatus;
import com.example.EkoBikeRental.util.ContextHolder;
import com.example.EkoBikeRental.util.SwitchScreenUtil;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

@Component
public class UserSearchParkingLotController extends SearchParkingLotController {
	
    @Autowired
    private SwitchScreenUtil switchScreenUtil;
	
    @Autowired
    private UserParkingLotService userParkingLotService;
	
    @FXML
    private Button btnReturnBike;

    @FXML
    private Button btnViewInfo;
    
    @FXML
    private void initialize() {
    	initSearchParkingLot();
    	updateButtonBar();
    }
    
	protected void updateButtonBar() {
    	tblParkingLot.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
    		if(newValue!=null) {
    			btnViewInfo.setDisable(false);
    			if(newValue.emptyDockProperty().intValue() > 0 && userParkingLotService.getUserStatus() == UserStatus.RENTING) { 
    				btnReturnBike.setDisable(false);
    			} else btnReturnBike.setDisable(true);
    		} else{
    			btnViewInfo.setDisable(true);
    			btnReturnBike.setDisable(true);
    		}
    	});
	}

    @FXML
    private void btnReturnBikePressed(ActionEvent event) throws IOException {
    	ParkingLot pl = tblParkingLot.getSelectionModel().getSelectedItem();
		ContextHolder.setReturnParkingLotId(pl.getId());
		switchScreenUtil.popUpReturnPayment(event);
    }

    @FXML
    private void btnViewInforPressed(ActionEvent event) throws IOException {
    	ParkingLot pl = tblParkingLot.getSelectionModel().getSelectedItem();
    	switchScreenUtil.switchViewParkingLotScene(event, pl);
    }
    
    @FXML
	protected void btnBackPressed(ActionEvent event) throws IOException {
    	switchScreenUtil.switchScene(switchScreenUtil.MAIN_SCREEN, event);
    }
    
}
