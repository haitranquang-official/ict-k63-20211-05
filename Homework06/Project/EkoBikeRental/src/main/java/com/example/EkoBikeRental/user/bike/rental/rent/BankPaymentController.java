package com.example.EkoBikeRental.user.bike.rental.rent;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import com.example.EkoBikeRental.bank.BankAccount;
import com.example.EkoBikeRental.entities.Bike;
import com.example.EkoBikeRental.entities.BikeStatus;
import com.example.EkoBikeRental.entities.Record;
import com.example.EkoBikeRental.entities.User;
import com.example.EkoBikeRental.repositories.UserRepository;
import com.example.EkoBikeRental.util.ContextHolder;
import com.example.EkoBikeRental.util.SwitchScreenUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

@Component
public class BankPaymentController implements Initializable {

    @Autowired
    private BankPaymentService bankPaymentService;

    @Autowired
    public SwitchScreenUtil switchScreenUtil;

    @FXML
    private TextField UserValue;

    @FXML
    private Button TotalFee;

    @FXML
    private Button backButton;

    @FXML
    private TextField StartTime;

    @FXML
    private TextField DepositFee;

    @FXML
    private TextField inputPasswordTextField;

    @FXML
    private TextField ElapsedTime;

    @FXML
    private Text BikeName;

    @FXML
    private Button confirmButton;

    @FXML
    private TextField BikeValue;

    @FXML
    private TextField inputCardIDTextField;

    @FXML
    private Text BikeCode;


    private Record record;

    private BankAccount bankAccount = null;

    @FXML
    void backHandler(ActionEvent event) throws IOException {
        switchScreenUtil.switchScene(switchScreenUtil.RENT_BIKE_PAGE, event);
    }

    @FXML
    void confirmHandler(ActionEvent event) throws Exception {
        try{
            String accountNum = inputCardIDTextField.getText();
            String password = inputPasswordTextField.getText();
            if(accountNum.isBlank()){
                throw new Exception("Account ID empty");
            }
            if(password.isBlank()){
                throw new Exception("Password empty");
            }
            bankAccount = bankPaymentService.checkLogin(accountNum, password);
            if(bankPaymentService.checkBalance(bankAccount.getBalance(), record.getBike().calculateDepositFee()) == false){
                throw new Exception("Not enough balance");
            }
            record.setStartTime(LocalDateTime.now());
            ContextHolder.accountNum = bankAccount.getAccountNum();
            JOptionPane.showMessageDialog(null, "Bank Account Valid");
            this.bankPaymentService.doTransaction(record, bankAccount.getAccountNum(), bankAccount.getPassword());
            JOptionPane.showMessageDialog(null, "Rent bike successful");
            switchScreenUtil.switchToReturnBikeScreen(event);
        }
        catch(Exception e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, e.getMessage());
            return;
        }
        
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        
    }

    public void showRecordInfo(Bike bike){
        record = new Record();
        User user = bankPaymentService.getCurrentUser();
        if(user == null){
            JOptionPane.showMessageDialog(null, "user null error");
            return;
        }
        record.setIsRent(false);
        record.setUser(user);
        this.record.setBike(bike);
        this.UserValue.setText(user.getAccount());
        this.StartTime.setText(LocalDateTime.now().toString());
        this.DepositFee.setText(String.valueOf(record.getBike().calculateDepositFee()));
        this.BikeValue.setText(bike.getType().toString());
        this.TotalFee.setText(this.DepositFee.getText());
    }


    
}
