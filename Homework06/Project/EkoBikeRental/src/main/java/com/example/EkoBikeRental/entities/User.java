package com.example.EkoBikeRental.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.example.EkoBikeRental.bank.BankAccount;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="user_id")
    private Long id;

    private String account;

    private String password;
    
    @Enumerated(EnumType.STRING)
    private UserStatus status = UserStatus.NOT_RENTING;

    @OneToMany(mappedBy = "user")
    @JsonManagedReference(value = "user - record")
    private List<Record> records;    


    public User(){};

    
    // public User(Long id, String account, String password, List<Record> records, BankAccount bankAccount) {
    //     this.id = id;
    //     this.account = account;
    //     this.password = password;
    //     this.records = records;
    // }


    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }


    public String getPassword() {
        return password;
    }


    public void setPassword(String password) {
        this.password = password;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserStatus getStatus() {
		return status;
	}


	public void setStatus(UserStatus status) {
		this.status = status;
	}


	public List<Record> getRecords() {
        return records;
    }

    public void setRecords(List<Record> records) {
        this.records = records;
    }

    
}
