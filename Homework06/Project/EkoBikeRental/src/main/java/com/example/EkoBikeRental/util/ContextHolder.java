package com.example.EkoBikeRental.util;

import org.springframework.stereotype.Component;

@Component
public class ContextHolder {
    public static Long ID;
    public static AccountType accountType = AccountType.USER;
    public static Double sizeX = 1280.0, sizeY = 720.0;
    public static String accountNum = "";
    public static Long returnParkingLotId;
    
    public static Long getID() {
        return ID;
    }
    public static void setID(Long userID) {
        ContextHolder.ID = userID;
    }
    public static Long getReturnParkingLotId() {
        return returnParkingLotId;
    }
    public static void setReturnParkingLotId(Long returnParkingLotId) {
        ContextHolder.returnParkingLotId = returnParkingLotId;
    }
    public static AccountType getAccountType() {
        return accountType;
    }
    public static void setAccountType(AccountType accountType) {
        ContextHolder.accountType = accountType;
    }
    public static void logOut() {
        ContextHolder.ID = null;
        ContextHolder.accountType = null;
        ContextHolder.accountNum = null;
    }
    
}
