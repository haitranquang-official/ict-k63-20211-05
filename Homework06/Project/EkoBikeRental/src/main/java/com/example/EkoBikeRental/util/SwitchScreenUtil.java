package com.example.EkoBikeRental.util;

import java.io.IOException;

import com.example.EkoBikeRental.admin.bike.create.AddNewBikeController;
import com.example.EkoBikeRental.admin.bike.edit.EditBikeController;
import com.example.EkoBikeRental.entities.Bike;
import com.example.EkoBikeRental.entities.ParkingLot;
import com.example.EkoBikeRental.user.bike.rental.rent.BankPaymentController;
import com.example.EkoBikeRental.user.bike.rental.rent.RentBikePageController;
import com.example.EkoBikeRental.user.bike.rental.returnbike.ReturnBikePageController;
import com.example.EkoBikeRental.user.bike.rental.returnbike.ReturnPaymentController;
import com.example.EkoBikeRental.user.parkinglot.UserViewParkingLotController;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Stage;

@Component
public class SwitchScreenUtil {
    private ApplicationContext applicationContext;
    private Parent root;
	private Scene scene;
	private Stage stage;

    @Value("classpath:/fxml/MainScreen.fxml")
    public Resource MAIN_SCREEN;

    @Value("classpath:/fxml/CreateParkingLot.fxml")
    public Resource CREATE_PARKING_LOT_PAGE;

    @Value("classpath:/fxml/LoginForm.fxml")
    public Resource LOGIN_PAGE;

    @Value("classpath:/fxml/BankPaymentScene.fxml")
    public Resource BANK_PAYMENT_SCENE;
    
    @Value("classpath:/fxml/RentBikePane.fxml")
    public Resource RENT_BIKE_PAGE;

    @Value("classpath:/fxml/PaymentPopup.fxml")
    public Resource PAYMENT_POPUP;

    @Value("classpath:/fxml/CalculationScreen.fxml")
    public Resource RETURN_BIKE_PAGE;

    @Value("classpath:/fxml/ReturnPaymentScreen.fxml")
    public Resource RETURN_PAYMENT_PAGE;
    
    @Value("classpath:/fxml/UserSearchParkingLot.fxml")
    public Resource USER_SEARCH_PARKING_LOT_PAGE;
    
    @Value("classpath:/fxml/UserViewParkingLot.fxml")
    public Resource USER_VIEW_PARKING_LOT_PAGE;

	@Value("classpath:/fxml/AdminMainPage.fxml")
	public Resource ADMIN_MAIN_PAGE;

	@Value("classpath:/fxml/MainScreen.fxml")
	public Resource USER_MAIN_PAGE;

	@Value("classpath:/fxml/AdminSearchParkingLot.fxml")
	public Resource ADMIN_SEARCH_PARKING_LOT_PAGE;
	
	@Value("classpath:/fxml/AdminViewBike.fxml")
	public Resource ADMIN_VIEW_BIKE_PAGE;
	
	@Value("classpath:/fxml/AdminABikePage.fxml")
	public Resource ADMIN_A_BIKE_PAGE;

    public SwitchScreenUtil(ApplicationContext applicationContext)
    {
        this.applicationContext = applicationContext;
    }
    public void switchScene(Resource resource, Event event) throws IOException {
		FXMLLoader loader = new FXMLLoader(resource.getURL());
		loader.setControllerFactory(param -> applicationContext.getBean(param));

		root = loader.load();
		scene = new Scene(root, ContextHolder.sizeX, ContextHolder.sizeY);
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.setScene(scene);
	}

    public void switchBankPaymentScene(Event event, Bike bike) throws IOException {
        FXMLLoader loader = new FXMLLoader(BANK_PAYMENT_SCENE.getURL());
		loader.setControllerFactory(param -> applicationContext.getBean(param));
        root = loader.load();
        BankPaymentController controller = loader.getController();
        controller.showRecordInfo(bike);
		scene = new Scene(root, ContextHolder.sizeX, ContextHolder.sizeY);
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.setScene(scene);
    }
    
    public void switchViewParkingLotScene(Event event, ParkingLot pl) throws IOException {
		FXMLLoader loader = new FXMLLoader(USER_VIEW_PARKING_LOT_PAGE.getURL());
		loader.setControllerFactory(param -> applicationContext.getBean(param));

		root = loader.load();
		UserViewParkingLotController controller = loader.getController();
		controller.initialize(pl);
		scene = new Scene(root, ContextHolder.sizeX, ContextHolder.sizeY);
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.setScene(scene);
	}
    
    public void switchViewToRentBikeScene(Event event, Long bikeID) throws IOException {
		FXMLLoader loader = new FXMLLoader(RENT_BIKE_PAGE.getURL());
		loader.setControllerFactory(param -> applicationContext.getBean(param));

		root = loader.load();
		RentBikePageController controller = loader.getController();
		controller.init(bikeID);
		scene = new Scene(root, ContextHolder.sizeX, ContextHolder.sizeY);
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.setScene(scene);
	}

    public void switchToReturnBikeScreen(Event event) throws IOException{
        FXMLLoader loader = new FXMLLoader(RETURN_BIKE_PAGE.getURL());
		loader.setControllerFactory(param -> applicationContext.getBean(param));

		root = loader.load();
		ReturnBikePageController controller = loader.getController();
		controller.init();
		scene = new Scene(root, ContextHolder.sizeX, ContextHolder.sizeY);
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.setScene(scene);
    }

    public void switchToPaymentReturnScreen(Event event) throws IOException{
        FXMLLoader loader = new FXMLLoader(RETURN_PAYMENT_PAGE.getURL());
		loader.setControllerFactory(param -> applicationContext.getBean(param));

		root = loader.load();
		ReturnPaymentController controller = loader.getController();
		// controller.init(userId);
		scene = new Scene(root, ContextHolder.sizeX, ContextHolder.sizeY);
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.setScene(scene);
    }

	public void popUpReturnPayment(ActionEvent event) throws IOException{
		FXMLLoader loader = new FXMLLoader(RETURN_PAYMENT_PAGE.getURL());
		loader.setControllerFactory(param -> applicationContext.getBean(param));

		root = loader.load();
		ReturnPaymentController controller = loader.getController();
		controller.initEvent(event);
		scene = new Scene(root, 400, 280);
		stage = new Stage();
		stage.setScene(scene);
		stage.show();
	}
	
	public void switchAdminEditBike(Event event, EditBikeController editBikeController, Bike bike) throws IOException {
		FXMLLoader loader = new FXMLLoader(ADMIN_A_BIKE_PAGE.getURL());
		loader.setController(editBikeController);
		loader.setControllerFactory(param -> applicationContext.getBean(param));

		root = loader.load();
		EditBikeController controller = loader.getController();
		controller.fillInfo(bike);
		scene = new Scene(root, ContextHolder.sizeX, ContextHolder.sizeY);
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.setScene(scene);
	}

	public void switchAddNewBike(Event event, AddNewBikeController addNewBikeController) throws IOException {
        FXMLLoader loader = new FXMLLoader(ADMIN_A_BIKE_PAGE.getURL());
        loader.setController(addNewBikeController);
		loader.setControllerFactory(param -> applicationContext.getBean(param));
        root = loader.load();

		scene = new Scene(root, ContextHolder.sizeX, ContextHolder.sizeY);
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.setScene(scene);
    }
	
}
