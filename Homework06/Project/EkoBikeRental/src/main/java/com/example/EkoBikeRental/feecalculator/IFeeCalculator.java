package com.example.EkoBikeRental.feecalculator;

import org.springframework.stereotype.Component;

public interface IFeeCalculator {
    public Double calculate(Integer minutes);
}
