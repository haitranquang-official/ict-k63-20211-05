package com.example.EkoBikeRental.entities;

public enum BikeType {
    ELECTRICAL_BIKE,
    NORMAL_BIKE,
    TWIN_BIKE
}
