package com.example.EkoBikeRental.admin.parkinglot.create;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import com.example.EkoBikeRental.entities.ParkingLot;
import com.example.EkoBikeRental.repositories.ParkingLotRepository;
import com.example.EkoBikeRental.util.SwitchScreenUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javafx.fxml.Initializable;

@Component
public class CreateParkingLotController {

    @Autowired
    private SwitchScreenUtil switchScreenUtil;

    private final CreateParkingLotService createParkingLotService;

    @FXML
    private Button createButton;

    @FXML
    private Button backButton;

    @FXML
    private TextField addressTextField;

    @FXML
    private TextField descriptionTextField;

    @FXML
    private TextField parkingLotNameTextField;

    @FXML
    private Button viewParkingLotButton;

    @FXML
    private TextField slotTextField;

    @Autowired
    public CreateParkingLotController(CreateParkingLotService createParkingLotService) {
        this.createParkingLotService = createParkingLotService;
    }

    public void addParkingLot(ParkingLot parkingLot) throws Exception {
        this.createParkingLotService.addParkingLot(parkingLot);
    }

    public List<ParkingLot> getAllParkingLot() {
        return this.createParkingLotService.findAll();
    }

    public void validateInput() throws Exception {
        if (addressTextField.getText().isEmpty()) {
            throw new Exception("Parking Lot address can not be empty");
        }
        if (parkingLotNameTextField.getText().isEmpty()) {
            throw new Exception("Parking Lot name can not be empty");
        }
        try {
            if ((slotTextField.getText().isEmpty()) || (Integer.valueOf(slotTextField.getText()) < 0)) {
                throw new Exception("Parking Lot slot must be a non negative integer");
            }
        } catch (NumberFormatException e) {
            throw new Exception("Parking slot must be a non negative integer");
        }

    }

    @FXML
    public void backButtonHandler(ActionEvent event) throws IOException {
        this.switchScreenUtil.switchScene(switchScreenUtil.ADMIN_SEARCH_PARKING_LOT_PAGE, event);
    }

    @FXML
    public void createButtonHandler(ActionEvent event) {
        try {
            validateInput();
            ParkingLot parkingLot = new ParkingLot(parkingLotNameTextField.getText().trim(),
                    addressTextField.getText().trim(),
                    Integer.valueOf(slotTextField.getText().trim()));
            parkingLot.setDescription(descriptionTextField.getText());
            addParkingLot(parkingLot);
            JOptionPane.showMessageDialog(null, "Parking Lot added: " + parkingLot.toString());
            clearAllTextField();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    @FXML
    public void viewParkingLotButtonHandler(ActionEvent event) throws IOException {
        this.switchScreenUtil.switchScene(switchScreenUtil.ADMIN_SEARCH_PARKING_LOT_PAGE, event);
    }

    public void clearAllTextField()
    {
        parkingLotNameTextField.clear();
        addressTextField.clear();
        slotTextField.clear();
        descriptionTextField.clear();
    }
}
