package com.example.EkoBikeRental.depositcalculator;

public interface IDepositCalculator {
    public Double getDepositFee();
}
