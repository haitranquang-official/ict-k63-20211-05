package com.example.EkoBikeRental.admin.bike.edit;

import javax.swing.JOptionPane;

import org.springframework.stereotype.Component;

import com.example.EkoBikeRental.admin.bike.AdminABikePageController;
import com.example.EkoBikeRental.entities.Bike;
import com.example.EkoBikeRental.entities.ParkingLot;

import javafx.event.ActionEvent;

@Component
public class EditBikeController extends AdminABikePageController{	
	ParkingLot oldParkingLot;
	
	@Override
	protected void setText() {
		titleLbl.setText("Edit Bike");
    	funcBtn.setText("Apply");
	}

	@Override
	protected void checkValidity() throws Exception {
		try {
			if(!licenseTf.getText().equals(bike.getLicensePlate())) checkValidLicense(licenseTf.getText());
			if(!parkingLotComboBox.getValue().equals(bike.getParkingLot())) checkValidParkingLot(parkingLotComboBox.getValue());
	    	checkNotRequiredInfo();
	    } catch (Exception e) {
	        JOptionPane.showMessageDialog(null, e.getMessage(), "Alert", JOptionPane.WARNING_MESSAGE);
	        throw new Exception();
	    }
	}
	
	@Override
	protected void funcBtnHandler(ActionEvent event) {
		updateBikeHandler(event);
	}
	
	private void updateBikeHandler(ActionEvent event) {
		oldParkingLot = bike.getParkingLot();
    	String oldBike = bike.toString();
    	try {
    		checkValidity();
    	} catch(Exception e) {
    		return;
    	}
        bike.setType(typeComboBox.getValue());
        setBikeInfo();
        
        if (oldBike.equals(bike.toString()) && oldParkingLot.equals(bike.getParkingLot())) {
        	JOptionPane.showMessageDialog(null, "Nothing to update", "Message", JOptionPane.INFORMATION_MESSAGE);
        	return;
        }
        else {
        	String question = new String("Are you sure to update the information?");
            String message = new String("Successfully updated bike's information.\n");
            checkConfirmation(event, bike, question, message);
        } 
    }
	
	@Override
	protected void updateDatabase() throws Exception {
		bikeUpdatePLService.removeABikeFromPl(oldParkingLot);
		bikeUpdatePLService.addABikeToParkingLot(bike.getParkingLot());
		bikeUpdateService.updateBikeDb(bike);
	}
	
	public void fillInfo(Bike bike) {
		this.bike = bike;
		typeComboBox.setValue(bike.getType());
		if(bike.getName() != null) nameTf.setText(bike.getName());
		if(bike.getWeight() != null) weightTf.setText(bike.getWeight().toString());
		licenseTf.setText(bike.getLicensePlate());
    	if(bike.getProducer() != null) producerTf.setText(bike.getProducer());
    	if(bike.getManufacturingDate() != null) datePicker.setValue(bike.getManufacturingDate());
    	if(bike.getCost() != null) costTf.setText(bike.getCost().toString());
    	parkingLotComboBox.setValue(bike.getParkingLot());
	}
}
