package com.example.EkoBikeRental.entities;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.criteria.Predicate.BooleanOperator;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table
public class Record {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="record_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "user_id", nullable = true)
    @JsonBackReference(value = "user - record")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "bike_id", nullable = true)
    @JsonBackReference(value = "bike - record")
    private Bike bike;

    @Column(name="start_time")
    private LocalDateTime startTime = LocalDateTime.now();

    @Column(name="end_time")
    private LocalDateTime endTime;

    @Column(name="cost")
    private Double cost;

    @Column(name="is_rent")
    private Boolean isRent;

    // public Record(Long id, User user, Bike bike, LocalDateTime endTime, Double cost) {
    //     this.id = id;
    //     this.user = user;
    //     this.bike = bike;
    //     this.endTime = endTime;
    //     this.cost = cost;
    // }

    public Record(){};

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Bike getBike() {
        return bike;
    }

    public void setBike(Bike bike) {
        this.bike = bike;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Boolean getIsRent() {
        return isRent;
    }

    public void setIsRent(Boolean isRent) {
        this.isRent = isRent;
    }

    
}
