package com.example.EkoBikeRental.feecalculator;

public class NormalFeeCalculator implements IFeeCalculator{

    @Override
    public Double calculate(Integer minutes) {
        double base1 = 10, base2 = 30;
        if(minutes < base1) return (double) 0;
        if(minutes < base2) return (double) 10000;
        return 10000 + Math.ceil((minutes - base2) / 15) * 3000;
    }
}
