package com.example.EkoBikeRental.abstracts;

import java.time.LocalDateTime;

import com.example.EkoBikeRental.entities.Bike;
import com.example.EkoBikeRental.entities.BikeStatus;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public abstract class BikeTableController {

    @FXML
	public TableView<Bike> tblBike;
    
    @FXML
    protected TableColumn<Bike, String> colName;

    @FXML
    protected TableColumn<Bike, Double> colWeight;

    @FXML
    protected TableColumn<Bike, Double> colCost;

    @FXML
    protected TableColumn<Bike, LocalDateTime> colDate;

    @FXML
    protected TableColumn<Bike, String> colType;

    @FXML
    protected TableColumn<Bike, String> colLicense;
    
    @FXML
    protected TableColumn<Bike, Long> colId;

    @FXML
    protected TableColumn<Bike, String> colProducer;
    
    @FXML
    protected TableColumn<Bike, BikeStatus> colStatus;
    
    public void initBikeTable(ObservableList<Bike> observableList) {
    	showCellValue();
    	tblBike.setItems(observableList);
    }
    
    private void showCellValue() {
    	colName.setCellValueFactory(new PropertyValueFactory<>("name"));
    	colId.setCellValueFactory(new PropertyValueFactory<>("id"));
    	colStatus.setCellValueFactory(new PropertyValueFactory<>("bikeStatus"));
    	
    	tblBike.setRowFactory(row -> new TableRow<Bike>() {
    	    @Override
    	    protected void updateItem(Bike bike, boolean empty) {
    	        super.updateItem(bike, empty);
    	        if (bike == null || bike.getClass() == null) {
    	        	setStyle("");
    	        } else if (bike.getBikeStatus().equals(BikeStatus.BUSY)) {
    	            setStyle("-fx-background-color: orange");
    	        } else if (bike.getBikeStatus().equals(BikeStatus.NOT_IN_USE)) {
    	            setStyle("-fx-background-color: red");
    	        } else setStyle("");
    	    }
    	});
    	
    	colType.setCellValueFactory(new PropertyValueFactory<>("type"));
    	colWeight.setCellValueFactory(new PropertyValueFactory<>("weight"));
    	colLicense.setCellValueFactory(new PropertyValueFactory<>("licensePlate"));
    	colDate.setCellValueFactory(new PropertyValueFactory<>("manufacturingDate"));
    	colProducer.setCellValueFactory(new PropertyValueFactory<>("producer"));
    	colCost.setCellValueFactory(new PropertyValueFactory<>("cost"));
    }
}
