package com.example.EkoBikeRental.entities;

public enum UserStatus {
	RENTING,
	NOT_RENTING
}
