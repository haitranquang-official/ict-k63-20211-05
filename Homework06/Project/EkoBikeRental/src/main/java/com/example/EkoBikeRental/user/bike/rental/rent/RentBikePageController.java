package com.example.EkoBikeRental.user.bike.rental.rent;
import java.io.IOException;
import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import com.example.EkoBikeRental.entities.Bike;
import com.example.EkoBikeRental.entities.BikeStatus;
import com.example.EkoBikeRental.entities.UserStatus;
import com.example.EkoBikeRental.util.SwitchScreenUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

@Component
public class RentBikePageController implements Initializable {

    @Autowired
    private RentBikeService rentBikeService;

    @Autowired
    private SwitchScreenUtil switchScreenUtil;

    @FXML
    private Button payButton;

    @FXML
    private Button HomeButton;

    @FXML
    private TextField BikeTypeValue;

    @FXML
    private AnchorPane rentBikePage;

    @FXML
    private Button checkButton;

    @FXML
    private Text BikeName;

    @FXML
    private Text BikeCode;

    @FXML
    private TextField BikeNameValue;

    @FXML
    private TextField ParkingLotAddress;

    @FXML
    private TextField inputBikeIDTextField;

    @FXML
    private TextField ProducerValue;

    @FXML
    private Button backButton;

    @FXML
    private TextField ProducerDateValue;

    @FXML
    private TextField DepositFee;

    private Bike bike;

    @FXML
    void payHandler(ActionEvent event) throws IOException {
        if(bike == null){
            JOptionPane.showMessageDialog(null, "bike null");
            return;
        }
        switchScreenUtil.switchBankPaymentScene(event, bike);
    }
    
    private void checkAction(){
        String text = inputBikeIDTextField.getText();
        if(text.isBlank()){
            JOptionPane.showMessageDialog(null, "Blank text field");
            return;
        }
        try {
            bike = rentBikeService.findBikeByBikeID(getBikeID());
            BikeStatus bikeStatus = rentBikeService.checBikeStatus(bike);
            if(bikeStatus.equals(BikeStatus.BUSY)){
				throw new Exception("This bike has already been rented" + 
										"\nPlease choose another one" +
										"\n(or please wait if you love it too much)");
			}
			else if(bikeStatus.equals(BikeStatus.NOT_IN_USE)){
				throw new Exception("This bike is currently not in use");
			}
            showBikeInfo(bike);
            
        } catch (Exception e) {
            // e.printStackTrace();
            JOptionPane.showMessageDialog(null, e.getMessage());
            return;
        }
    }

    public void showBikeInfo(Bike bike){
        
        this.BikeNameValue.setText(bike.getName());
        this.BikeTypeValue.setText(bike.getType().toString());
        this.ProducerValue.setText(bike.getProducer());
        this.ProducerDateValue.setText(bike.getManufacturingDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        this.ParkingLotAddress.setText(bike.getParkingLot().getAddress());
        this.DepositFee.setText(String.valueOf(bike.calculateDepositFee()));
        inputBikeIDTextField.setEditable(false);
    }

    @FXML
    void checkHandler(ActionEvent event) {
        checkAction();
    }

    @FXML
    void homeButtonHandler(ActionEvent event) throws IOException {
        switchScreenUtil.switchScene(switchScreenUtil.MAIN_SCREEN, event);
    }

    @FXML
    void backHandler(ActionEvent event) throws IOException {
    	switchScreenUtil.switchScene(switchScreenUtil.USER_SEARCH_PARKING_LOT_PAGE, event);
    }
    

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        this.rentBikePage.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent key) {
                if(key.getCode().equals(KeyCode.ENTER)){
                    checkAction();
                }     
            }
            
        });
        this.inputBikeIDTextField.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                inputBikeIDTextField.setEditable(true);
            }
        });

    }

    private Long getBikeID() throws Exception{
        String text = inputBikeIDTextField.getText();
        try{
            return Long.valueOf(text);
        }
        catch (NumberFormatException e) {
            throw new Exception("Invalid bike id");
        }
        
    }

	public void init(Long bikeID) {
		this.inputBikeIDTextField.setText(String.valueOf(bikeID));
	}

}