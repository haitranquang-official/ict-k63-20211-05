package com.example.EkoBikeRental.abstracts;

import com.example.EkoBikeRental.entities.ParkingLot;

import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public abstract class ParkingLotTableController {

	@FXML
	protected TableColumn<ParkingLot, String> colName;

    @FXML
    protected TableColumn<ParkingLot, Integer> colTwinBike;
    
    @FXML
    protected TableColumn<ParkingLot, Integer> colBike;

    @FXML
    protected TableColumn<ParkingLot, String> colAddress;

    @FXML
    protected TableColumn<ParkingLot, Integer> colDock;

    @FXML
    protected TableColumn<ParkingLot, Integer> colEBike;

    @FXML
    protected TableView<ParkingLot> tblParkingLot;
    
    public void initParkingLotTable() {
    	showCellValue();
    }
    
    private void showCellValue() {
    	colName.setCellValueFactory(new PropertyValueFactory<>("name"));
    	colAddress.setCellValueFactory(new PropertyValueFactory<>("address"));
    	colDock.setCellValueFactory(cellData ->cellData.getValue().emptyDockProperty().asObject());
    	colBike.setCellValueFactory(cellData ->cellData.getValue().normalBikeProperty().asObject());
    	colEBike.setCellValueFactory(cellData ->cellData.getValue().eBikeProperty().asObject());
    	colTwinBike.setCellValueFactory(cellData ->cellData.getValue().twinBikeProperty().asObject());
    }
}
