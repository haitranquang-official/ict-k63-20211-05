package com.example.EkoBikeRental.admin.bike;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import javax.swing.JOptionPane;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.EkoBikeRental.admin.bike.service.BikeCheckInfoService;
import com.example.EkoBikeRental.admin.bike.service.BikeCheckPLService;
import com.example.EkoBikeRental.admin.bike.service.BikeUpdatePLService;
import com.example.EkoBikeRental.admin.bike.service.BikeUpdateService;
import com.example.EkoBikeRental.entities.Bike;
import com.example.EkoBikeRental.entities.BikeType;
import com.example.EkoBikeRental.entities.ParkingLot;
import com.example.EkoBikeRental.util.SwitchScreenUtil;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

@Component
public abstract class AdminABikePageController {
	
	protected Bike bike;
	
	@Autowired
	protected SwitchScreenUtil switchScreenUtil;
	
	@Autowired
	protected BikeCheckInfoService bikeCheckInfoService;
	
	@Autowired
	protected BikeUpdateService bikeUpdateService;
	
	@Autowired
	protected BikeUpdatePLService bikeUpdatePLService;
	
	@Autowired
	protected BikeCheckPLService bikeCheckPLService;
	
	@FXML
	protected Label titleLbl;
	
	@FXML
	protected TextField weightTf;

    @FXML
    protected TextField nameTf;

    @FXML
    protected TextField licenseTf;

    @FXML
    protected ComboBox<BikeType> typeComboBox;

    @FXML
    protected TextField producerTf;

    @FXML
    protected DatePicker datePicker;

    @FXML
    protected TextField costTf;
    
    @FXML
    protected ComboBox<ParkingLot> parkingLotComboBox;
    
    @FXML
    protected Button returnBtn;
    
    @FXML
    protected Button funcBtn;
    
    @FXML
    private void returnHandler(ActionEvent event) throws IOException {
        this.switchScreenUtil.switchScene(switchScreenUtil.ADMIN_VIEW_BIKE_PAGE, event);
    }
    
    @FXML
    protected abstract void funcBtnHandler(ActionEvent event);
    
    @FXML
    private void initialize() {
    	init();
    	setText();
    	setBikeType();
	}
    
    private void init() {
    	ObservableList<BikeType> bikeTypeList = FXCollections.observableArrayList(BikeType.NORMAL_BIKE, BikeType.ELECTRICAL_BIKE, BikeType.TWIN_BIKE);
    	typeComboBox.setItems(bikeTypeList);
    	
    	ObservableList<ParkingLot> parkingLotList = FXCollections.observableArrayList(bikeCheckPLService.findAll());
    	parkingLotComboBox.setItems(parkingLotList);
    	
    	datePicker.setDayCellFactory(param -> new DateCell() {
            @Override
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                setDisable(empty || date.compareTo(LocalDate.now()) > 0 );
            }
        });
    	
    	limitInputLength();
    }
    
    protected abstract void setText();
    
    protected void setBikeType() {}
    
    protected abstract void checkValidity() throws Exception;
    
    protected void checkValidLicense(String license) throws Exception {
    	bikeCheckInfoService.checkValidLicense(license);
    }
    
    protected void checkValidParkingLot(ParkingLot parkingLot) throws Exception {
    	bikeCheckPLService.checkCanAddABike(parkingLot);
    }
    
    protected void checkNotRequiredInfo() throws Exception {
    	bikeCheckInfoService.checkValidWeight(weightTf.getText());
    	bikeCheckInfoService.checkValidCost(costTf.getText());
    	bikeCheckInfoService.checkValidDate(datePicker.getEditor().getText());
    	bikeCheckInfoService.checkNotFutureDate(datePicker.getEditor().getText());
    }
    
    protected void setBikeInfo() {    
    	bike.setName(!nameTf.getText().isEmpty()?nameTf.getText():null);
    	bike.setWeight(!weightTf.getText().isEmpty()?Double.valueOf(weightTf.getText()):null);
    	bike.setLicensePlate(licenseTf.getText());
    	bike.setProducer(!producerTf.getText().isEmpty()?producerTf.getText():null);
    	bike.setManufacturingDate(datePicker.getValue()!=null?datePicker.getValue():null);
    	bike.setCost(!costTf.getText().isEmpty()?Double.valueOf(costTf.getText()):null);
    	bike.setParkingLot(parkingLotComboBox.getValue());
    }
    
    protected void checkConfirmation(ActionEvent event, Bike bike, String question, String message) {
    	int opt = JOptionPane.showConfirmDialog(null, question, "Question", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
    	if (opt == JOptionPane.YES_OPTION) {
    		try {
    			updateDatabase();
        		JOptionPane.showMessageDialog(null, message + bike.toString() + "\n" + bike.getParkingLot().toString());
        		switchScreenUtil.switchScene(switchScreenUtil.ADMIN_VIEW_BIKE_PAGE, event);
    		} catch(Exception e) {
                e.printStackTrace();
            }	
    	}    
	}
 
    protected abstract void updateDatabase () throws Exception;
    
    public static void addTextLimiter(TextField tf, int maxLength) {
        tf.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(final ObservableValue<? extends String> ov, final String oldValue, final String newValue) {
                if (tf.getText().length() > maxLength) {
                    String s = tf.getText().substring(0, maxLength);
                    tf.setText(s);
                }
            }
        });
    }
    
    private void limitInputLength() {
    	addTextLimiter(nameTf,50);
    	addTextLimiter(weightTf,5);
    	addTextLimiter(licenseTf,20);
    	addTextLimiter(producerTf,50);
    	addTextLimiter(costTf,10);
    }
}
