package com.example.EkoBikeRental.admin.bike.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.EkoBikeRental.abstracts.BikeService;
import com.example.EkoBikeRental.entities.Bike;
import com.example.EkoBikeRental.entities.BikeStatus;
import com.example.EkoBikeRental.repositories.BikeRepository;

@Service
public class BikeUpdateService extends BikeService {

	public BikeUpdateService(BikeRepository bikeRepository) {
		super(bikeRepository);
	}
	
	@Transactional
	public void updateBikeDb(Bike bike) throws Exception {
        bikeRepository.save(bike);
    } 

    @Transactional
    public void deleteBike(Bike bike) throws Exception {
    	if(bike.getBikeStatus() == BikeStatus.BUSY) throw new Exception("This bike is being rented, cannot delete now");
    	else bikeRepository.delete(bike);
    }
}
