package com.example.EkoBikeRental.entities;

import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

@Entity
@Table(name = "parking_lot")
@JsonIgnoreProperties({"bikes"})
public class ParkingLot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String address;
    @Column(name = "number_of_slots")
    private Integer numberOfSlots = 0;
    private String description;
    @Column(name = "num_bike")
	private Integer numBike = 0;
    @OneToMany(mappedBy = "parkingLot", fetch = FetchType.LAZY)
    @JsonManagedReference(value = "parking_lot - bike")
    private List<Bike> bikes;
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    
    public Integer getNumberOfSlots() {
        return numberOfSlots;
    }
    public void setNumberOfSlots(Integer numberOfSlots) {
        this.numberOfSlots = numberOfSlots;
    }
    
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getNumBike() {
        return numBike;
    }
    public void setNumBike(Integer numBike) {
        this.numBike = numBike;
    }

    public ParkingLot() {
    }
    public ParkingLot(String name, String address, Integer numberOfSlots)
    {
        this.name = name;
        this.address = address;
        this.numberOfSlots = numberOfSlots;
        this.numBike = 0;
    }
    // public ParkingLot(Long id, String name, String address, Integer numberOfSlots, String description,
    //         Integer numBike) {
    //     this.id = id;
    //     this.name = name;
    //     this.address = address;
    //     this.numberOfSlots = numberOfSlots;
    //     this.description = description;
    //     this.numBike = numBike;
    // }
    @Override
    public String toString() {
        return "ParkingLot [address=" + address + ", description=" + description + ", id=" + id
                + ", name=" + name + ", numBike=" + numBike + ", numberOfSlots=" + numberOfSlots + "]";
    }
    
	public IntegerProperty normalBikeProperty() {
		IntegerProperty numBike = new SimpleIntegerProperty(0);
		for(Bike b : bikes) {
			if(b instanceof NormalBike) {
				numBike.setValue(numBike.getValue() + 1);
			}
		}
		return numBike;
	}
	
	public IntegerProperty eBikeProperty() {
		IntegerProperty numBike = new SimpleIntegerProperty(0);
		for(Bike b : bikes) {
			if(b instanceof EBike) {
				numBike.setValue(numBike.getValue() + 1);
			}
		}
		return numBike;
	}
	
	public IntegerProperty twinBikeProperty() {
		IntegerProperty numBike = new SimpleIntegerProperty(0);
		for(Bike b : bikes) {
			if(b instanceof TwinBike) {
				numBike.setValue(numBike.getValue() + 1);
			}
		}
		return numBike;
	}
	
	public IntegerProperty emptyDockProperty() {
		IntegerProperty emptyDock = new SimpleIntegerProperty(this.getNumberOfSlots());
		emptyDock.setValue(emptyDock.getValue() - this.getBikes().size());
		return emptyDock;
	}
	
	public List<Bike> getBikes() {
		return bikes;
	}
    
}
