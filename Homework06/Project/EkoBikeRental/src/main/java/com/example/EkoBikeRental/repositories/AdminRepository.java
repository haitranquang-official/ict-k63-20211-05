package com.example.EkoBikeRental.repositories;

import com.example.EkoBikeRental.entities.Admin;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminRepository extends JpaRepository<Admin,Long>{
    Admin findOneByAccount(String account);
}
